﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "100.KBL.900.90";
            string str1 = "12.kbl.900.90";
            string str2 = "10B.KBL.900.90";
            string strPatten = @"^\d{2,4}\.+KBL.\d{2,4}\.\d(,\d+)?$";
            bool re = Regex.IsMatch(str, strPatten, RegexOptions.IgnoreCase);
            Console.WriteLine(re);
        }
    }
}
