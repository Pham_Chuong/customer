﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lottery_Statistics
{
    class Player
    {
        private Double _totalMoneyXac2Digit;
        public Double totalMoneyXac2Digit
        {
            get { return _totalMoneyXac2Digit; }
            set { _totalMoneyXac2Digit = value; }
        }

        private Double _totalMoneyXac3Digit;
        public Double totalMoneyXac3Digit
        {
            get { return _totalMoneyXac3Digit; }
            set { _totalMoneyXac3Digit = value; }
        }

        private Double _totalMoneyXac4Digit;
        public Double totalMoneyXac4Digit
        {
            get { return _totalMoneyXac4Digit; }
            set { _totalMoneyXac4Digit = value; }
        }

        private Double _totalMoneyXac;
        public Double totalMoneyXac
        {
            get { return _totalMoneyXac; }
            set { _totalMoneyXac = value; }
        }

        private Double _totalMoneyWin2Digit;
        public Double totalMoneyWin2Digit
        {
            get { return _totalMoneyWin2Digit; }
            set { _totalMoneyWin2Digit = value; }
        }

        private Double _totalMoneyWin3Digit;
        public Double totalMoneyWin3Digit
        {
            get { return _totalMoneyWin3Digit; }
            set { _totalMoneyWin3Digit = value; }
        }

        private Double _totalMoneyWin4Digit;
        public Double totalMoneyWin4Digit
        {
            get { return _totalMoneyWin4Digit; }
            set { _totalMoneyWin4Digit = value; }
        }

        private Double _totalMoneyWin;
        public Double totalMoneyWin
        {
            get { return _totalMoneyWin; }
            set { _totalMoneyWin = value; }
        }

        private String _strMsg;
        public String strMsg
        {
            get { return _strMsg; }
            set { _strMsg = value; }
        }

        private String _result = String.Empty;
        public String result
        {
            get { return _result; }
            set { _result = value; }
        }

        private String _status;
        public String status
        {
            get { return _status; }
            set { _status = value; }
        }

    }
}
