﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using DevExpress.XtraEditors;
using System.Globalization;
using System.Collections;
using Excel = Microsoft.Office.Interop.Excel;

namespace Lottery_Statistics
{
    public partial class Form1 : DevExpress.XtraEditors.XtraForm
    {
        CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
        private Double totalMoneyWin2Digit;
        private Double totalMoneyWin3Digit;
        private Double totalMoneyWin4Digit;
        private Double totalMoneyXac2Digit;
        private Double totalMoneyXac3Digit;
        private Double totalMoneyXac4Digit;
        private Double totalMoneyXac;
        private Double totalMoneyWin;
        private MainProcess mp = new MainProcess();
        private String strMsg = String.Empty;

        public Form1()
        {
            InitializeComponent();
            this.btn_XF.Enabled = false;
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            strMsg = "Bạn có muốn thoát không?";
            if (MessageBox.Show(strMsg, "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                Application.Exit();
                return;
            }
        }

        private void btn_ChooseFile_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.FileName = String.Empty;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                MainProcess.pathFile = openFileDialog1.FileName;
                //display path file
                this.lb_FileNameVal.Text = MainProcess.pathFile;
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.BeginEdit(true);
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if ((e.Exception) is FormatException)
            {
                strMsg = "Giá trị tiền không đúng! \n Chỉ được nhập số!";
                if (MessageBox.Show(strMsg, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation) == DialogResult.OK)
                {
                    DataGridView view = (DataGridView)sender;
                    view.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                    view.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.ForeColor = Color.Red;
                    e.ThrowException = false;
                    return;
                }
            }
        }
        
        private void btn_Calc1_Click(object sender, EventArgs e)
        {
            this.btn_Calc1.Enabled = false;
            this.btn_Exit.Enabled = false;

            String region = String.Empty;
            if (String.IsNullOrEmpty(MainProcess.pathFile))
            {
                strMsg = "Bạn chưa chỉ định File. \nHãy kiễm tra lại!";
                MessageBox.Show(strMsg, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.btn_Calc1.Enabled = true;
                this.btn_Exit.Enabled = true;
                return;
            }

            if (!File.Exists(MainProcess.pathFile))
            {
                strMsg = "File không tồn tại. \nHãy kiễm tra lại!";
                MessageBox.Show(strMsg, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.btn_Calc1.Enabled = true;
                this.btn_Exit.Enabled = true;
                return;
            }

            if (dataGridView1.RowCount != 0)
            {
                strMsg = "Kết quả chạy lần trước sẽ bị xóa đi. \nBạn có muốn xóa hay không?";
                if (MessageBox.Show(strMsg, "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    dataGridView1.Rows.Clear();
                }
                else
                {
                    this.btn_Calc1.Enabled = true;
                    this.btn_Exit.Enabled = true;
                    return;
                }
            }

            this.lb_Re2Con.Text = String.Empty;
            this.lb_Re3Con.Text = String.Empty;
            this.lb_Re4Con.Text = String.Empty;
            this.lb_ReTC.Text = String.Empty;

            this.lb_ReTX2Con.Text = String.Empty;
            this.lb_ReTX3Con.Text = String.Empty;
            this.lb_ReTX4Con.Text = String.Empty;
            this.lb_ReTX.Text = String.Empty;

            if (rad_MB.Checked)
            {
                region = Constants.MB;
            }
            else
            {
                region = Constants.MN;
            }

            strMsg = mp.Calculate(region);
            if (!String.IsNullOrEmpty(strMsg))
            {
                MessageBox.Show(strMsg, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.btn_Calc1.Enabled = true;
                this.btn_Exit.Enabled = true;
                return;
            }

            //set so tien ve 0 truoc khi chay lan 2
            this.totalMoneyWin = 0;
            this.totalMoneyWin2Digit = 0;
            this.totalMoneyWin3Digit = 0;
            this.totalMoneyWin4Digit = 0;
            this.totalMoneyXac = 0;
            this.totalMoneyXac2Digit = 0;
            this.totalMoneyXac3Digit = 0;
            this.totalMoneyXac4Digit = 0;

            List<Player> listData = new List<Player>();
            BindingSource datasBindingSource = new BindingSource();
            foreach (KeyValuePair<int, Player> pair in mp.subProcess.listPlayer)
            {
                // update thông tin trước khi output
                pair.Value.result = pair.Value.result.Equals(String.Empty) ? pair.Value.result : pair.Value.result.ToString().Remove(pair.Value.result.ToString().LastIndexOf('-'));
                listData.Add(pair.Value);

                // tong tien xac
                this.totalMoneyXac2Digit += pair.Value.totalMoneyXac2Digit;
                this.totalMoneyXac3Digit += pair.Value.totalMoneyXac3Digit;
                this.totalMoneyXac4Digit += pair.Value.totalMoneyXac4Digit;
                pair.Value.totalMoneyXac = pair.Value.totalMoneyXac2Digit + pair.Value.totalMoneyXac3Digit + pair.Value.totalMoneyXac4Digit;
                this.totalMoneyXac += pair.Value.totalMoneyXac;
                // tong tien win
                this.totalMoneyWin2Digit += pair.Value.totalMoneyWin2Digit;
                this.totalMoneyWin3Digit += pair.Value.totalMoneyWin3Digit;
                this.totalMoneyWin4Digit += pair.Value.totalMoneyWin4Digit;
                pair.Value.totalMoneyWin = pair.Value.totalMoneyWin2Digit + pair.Value.totalMoneyWin3Digit + pair.Value.totalMoneyWin4Digit;
                this.totalMoneyWin += pair.Value.totalMoneyWin;
            }

            // display data player
            datasBindingSource.DataSource = listData;
            this.playerBindingSource = datasBindingSource;
            this.dataGridView1.DataSource = this.playerBindingSource;

            show_TongTien();

            this.btn_Exit.Enabled = true;
            this.btn_Calc1.Enabled = true;
            this.btn_XF.Enabled = true;
        }

        private void show_TongTien()
        {
            //display total tien xac
            this.lb_ReTX2Con.Text = this.totalMoneyXac2Digit.ToString("#,###", cul);
            this.lb_ReTX3Con.Text = this.totalMoneyXac3Digit.ToString("#,###", cul);
            this.lb_ReTX4Con.Text = this.totalMoneyXac3Digit.ToString("#,###", cul);
            this.lb_ReTX.Text = this.totalMoneyXac.ToString("#,###", cul);

            // cal and display tien trung
            this.lb_Re2Con.Text = this.totalMoneyWin2Digit.ToString("#,###", cul);
            this.lb_Re3Con.Text = this.totalMoneyWin3Digit.ToString("#,###", cul);
            this.lb_Re4Con.Text = this.totalMoneyWin4Digit.ToString("#,###", cul);
            this.lb_ReTC.Text = this.totalMoneyWin.ToString("#,###", cul);
        }

        private void btn_XF_Click(object sender, EventArgs e)
        {
            this.saveFileDialog1.FileName = String.Empty;
            String strfilename = String.Empty;
            if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                strfilename = saveFileDialog1.InitialDirectory + saveFileDialog1.FileName;
                if (mp.subProcess.isFileOpen(strfilename))
                {
                    strMsg = "File đang mở.\nHãy đóng file và thực hiện xuất lại!";
                    MessageBox.Show(strMsg, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else
            {
                return;
            }

            // new application
            Excel.Application app = new Excel.Application();

            // open output sample
            Excel.Workbook wb_format = app.Workbooks.Open(Directory.GetCurrentDirectory() + "\\output_sample.xlsx");

            // creating new WorkBook within Excel application
            Excel.Workbook wb_output = app.Workbooks.Add(Type.Missing);
            Excel.Worksheet ws_format = null;
            Excel.Worksheet ws_output = null;
            try
            {
                //Open the WorkSheet
                ws_format = (Excel.Worksheet)wb_format.Sheets["Format"];
                ws_output = (Excel.Worksheet)wb_output.Sheets["Sheet1"];
                //Copy from source to destination
                ws_format.Copy(ws_output, Type.Missing);
                foreach (Excel.Worksheet ws in wb_output.Worksheets)
                {
                    if (!ws.Name.Equals("Format"))
                    {
                        ws.Delete();
                    }
                }
                ws_output = (Excel.Worksheet)wb_output.Sheets["Format"];
                ws_output.Name = "Thống Kê" + DateTime.Now.ToString("(yyyyMMdd)");

                // Get kết quả tính được
                object[,] arr = new object[dataGridView1.Rows.Count, dataGridView1.Columns.Count];
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridView1.Columns.Count - 1; j++)
                    {
                        arr[i, j] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                    }
                }

                //Xuất kết quả tính được
                Excel.Range c1 = (Excel.Range)ws_output.Cells[5, 2];
                Excel.Range c2 = (Excel.Range)ws_output.Cells[4 + dataGridView1.Rows.Count, 5];
                Excel.Range range = ws_output.get_Range(c1, c2);
                range.Value = arr;
                Excel.Range RngToCopy = ws_output.get_Range("A5", Type.Missing);
                Excel.Range RngToFormat = ws_output.get_Range("A5", "A" + (4 + dataGridView1.Rows.Count));
                RngToCopy.AutoFill(RngToFormat, Excel.XlAutoFillType.xlFillCopy);

                RngToCopy = ws_output.get_Range("A5", "E5");
                RngToFormat = ws_output.get_Range("A5", "E" + (4 + dataGridView1.Rows.Count));
                RngToCopy.AutoFill(RngToFormat, Excel.XlAutoFillType.xlFillFormats);

                // xuat tong tien
                object[,] arr2 = new object[9, 3];
                arr2[0, 0] = "Tổng tiền xác 2 con:";
                arr2[0, 1] = (this.totalMoneyXac2Digit).ToString();
                arr2[0, 2] = "";
                arr2[1, 0] = "Tổng tiền xác 3 con:";
                arr2[1, 1] = (this.totalMoneyXac3Digit + this.totalMoneyXac4Digit ).ToString();
                arr2[1, 2] = "";
                arr2[2, 0] = "Tổng tiền xác:";
                arr2[2, 1] = ((this.totalMoneyXac2Digit * 75) + (this.totalMoneyXac3Digit * 66 + this.totalMoneyXac4Digit * 66)).ToString();
                arr2[2, 2] = "";

                arr2[3, 0] = "";
                arr2[3, 1] = "";
                arr2[3, 2] = "";

                arr2[4, 0] = "Số trúng 2 con:";
                arr2[4, 1] = (this.totalMoneyWin2Digit / 75).ToString();
                arr2[4, 2] = this.totalMoneyWin2Digit.ToString();
                arr2[5, 0] = "Số trúng 3 con:";
                arr2[5, 1] = (this.totalMoneyWin3Digit / 650 + this.totalMoneyWin4Digit / 6000).ToString();
                arr2[5, 2] = (this.totalMoneyWin3Digit + this.totalMoneyWin4Digit).ToString();

                Excel.Range c3 = (Excel.Range)ws_output.Cells[6 + dataGridView1.Rows.Count, 2];
                Excel.Range c4 = (Excel.Range)ws_output.Cells[14 + dataGridView1.Rows.Count, 4];
                Excel.Range range1 = ws_output.get_Range(c3, c4);
                range1.Value = arr2;

                // Xuất kết quả số số trong ngày
                Excel.Range result1 = ws_output.get_Range("RESULT1");
                Excel.Range result2 = ws_output.get_Range("RESULT2");
                object[,] arrResult;
                Boolean bln = false;
                foreach (KeyValuePair<String, ArrayList> pair in mp.subProcess.listResult)
                {
                    arrResult = new object[13, 1];
                    if (mp.subProcess.listResult.Count == 1)
                    {
                        arrResult[0, 0] = pair.Key;
                        arrResult[1, 0] = pair.Value[0].ToString();
                        arrResult[2, 0] = pair.Value[1].ToString();
                        arrResult[3, 0] = pair.Value[2].ToString() + " - "
                                        + pair.Value[3].ToString();
                        arrResult[4, 0] = "";
                        arrResult[5, 0] = pair.Value[4].ToString() + " - "
                                        + pair.Value[5].ToString() + " - "
                                        + pair.Value[6].ToString() + " - "
                                        + pair.Value[7].ToString() + " - "
                                        + pair.Value[8].ToString() + " - "
                                        + pair.Value[9].ToString();

                        arrResult[6, 0] = "";
                        arrResult[7, 0] = pair.Value[10].ToString() + " - "
                                        + pair.Value[11].ToString() + " - "
                                        + pair.Value[12].ToString() + " - "
                                        + pair.Value[13].ToString();
                        arrResult[8, 0] = "";
                        arrResult[9, 0] = pair.Value[14].ToString() + " - "
                                        + pair.Value[15].ToString() + " - "
                                        + pair.Value[16].ToString() + " - "
                                        + pair.Value[17].ToString() + " - "
                                        + pair.Value[18].ToString() + " - "
                                        + pair.Value[19].ToString();
                        arrResult[10, 0] = pair.Value[20].ToString() + " - "
                                        + pair.Value[21].ToString() + " - "
                                        + pair.Value[22].ToString();
                        arrResult[11, 0] = pair.Value[23].ToString() + " - "
                                        + pair.Value[24].ToString() + " - "
                                        + pair.Value[25].ToString() + " - "
                                        + pair.Value[26].ToString();
                        arrResult[12, 0] = "Không có";
                        result1.Value = arrResult;
                    }
                    else
                    {
                        arrResult[0, 0] = pair.Key;
                        arrResult[1, 0] = pair.Value[0].ToString();
                        arrResult[2, 0] = pair.Value[1].ToString();
                        arrResult[3, 0] = pair.Value[2].ToString();
                        arrResult[4, 0] = "";
                        arrResult[5, 0] = pair.Value[3].ToString() + " - "
                                        + pair.Value[4].ToString();
                        arrResult[6, 0] = "";
                        arrResult[7, 0] = pair.Value[5].ToString() + " - "
                                        + pair.Value[6].ToString() + " - "
                                        + pair.Value[7].ToString() + " - "
                                        + pair.Value[8].ToString() + " - "
                                        + pair.Value[9].ToString() + " - "
                                        + pair.Value[10].ToString() + " - "
                                        + pair.Value[11].ToString();
                        arrResult[8, 0] = "";
                        arrResult[9, 0] = pair.Value[12].ToString();
                        arrResult[10, 0] = pair.Value[13].ToString() + " - "
                                        + pair.Value[14].ToString() + " - "
                                        + pair.Value[15].ToString();
                        arrResult[11, 0] = pair.Value[16].ToString();
                        arrResult[12, 0] = pair.Value[17].ToString();

                        if (!bln)
                        {
                            result1.Value = arrResult;
                            bln = true;
                        }
                        else
                        {
                            result2.Value = arrResult;
                        }
                    }
                }

                if (saveFileDialog1.OverwritePrompt)
                {
                    app.DisplayAlerts = false;
                    try
                    {
                        // save the application
                        wb_output.SaveAs(strfilename, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                        var strMsg = "Xuất File thành công!";
                        if (MessageBox.Show(strMsg, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                        {
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception)
            {
                var strMsg = "Xuất File không thành công!";
                if (MessageBox.Show(strMsg, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
                {
                    return;
                }
            }
            finally
            {
                // Close workbook format
                if (wb_format != null)
                {
                    wb_format.Saved = true;
                    wb_format.Close(false, Type.Missing, Type.Missing);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(wb_format);
                    wb_format = null;
                }

                // Close workbook output
                if (wb_output != null)
                {
                    wb_output.Close(true, Type.Missing, Type.Missing);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(wb_output);
                    wb_output = null;
                }

                // Close application
                if (app != null)
                {
                    app.Quit();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                    app = null;
                }
            }
        }
    }
}