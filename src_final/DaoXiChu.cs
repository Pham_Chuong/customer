﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lottery_Statistics
{
    class DaoXiChu : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        private XiChu xichu = new XiChu();

        public void tinh(Player thisPlayer, String digitVal, Double money, Region regionData1 = null, Region regionData2 = null)
        {
            IList<string> perms = Permutation_Generation.Permutation(digitVal);

            foreach (var perm in perms)
            {
                xichu.tinh(thisPlayer, perm, money, regionData1, regionData2);
            }
        }
    }
}
