﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Net.NetworkInformation;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;

namespace Lottery_Statistics
{
    class SubProcess
    {
        private Dictionary<Int32, Player> _listPlayer;
        public Dictionary<Int32, Player> listPlayer
        {
            get { return _listPlayer; }
            set { _listPlayer = value; }
        }

        private Dictionary<string, ArrayList> _listResult;
        public Dictionary<string, ArrayList> listResult
        {
            get { return _listResult; }
            set { _listResult = value; }
        }

        public String checkConnection()
        {
            String strMsg = String.Empty;
            try 
            {
                Ping myPing = new Ping();
                String host1 = "www.google.com";
                String host2 = "xskt.com.vn";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host1, timeout, buffer, pingOptions);
                if (reply.Status != IPStatus.Success)
                {
                    strMsg = "Hãy kiễm tra lại kết nối Internet của bạn!!!"; 
                }

                reply = myPing.Send(host2, timeout, buffer, pingOptions);
                if (reply.Status != IPStatus.Success)
                {
                    strMsg = "Hãy kiễm tra lại kết nối Internet của bạn!!!";
                }
            }
            catch (Exception)
            {
                strMsg = "Hãy kiễm tra lại kết nối Internet của bạn!!!";
            }
            return strMsg;
        }

        //Read file txt
        public string readFile(string pathFile)
        {
            String strMsg = String.Empty;
             //Check if file exists in directory
            if (File.Exists(pathFile))
            {
                string[] lines = File.ReadAllLines(pathFile);
                this._listPlayer = new Dictionary<int, Player>();
                int countKey = 0;
                foreach (string line in lines)
                {
                    if (String.IsNullOrEmpty(line) || String.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }
                    Player playerTmp = new Player();
                    playerTmp.strMsg = line;
                    this._listPlayer.Add(++countKey, playerTmp);
                }
            }
            return strMsg;
        }

        public Boolean check_data(String a, String b)
        {
            if (a.Contains(b) == true)
            {
                return true;
            }
            return false;
        }

        public ArrayList get_data_rss(String urls, String date)
        {
            ArrayList list = new ArrayList();
            var feedUrl = urls;
            //XmlReader r = new MyXmlReader(feedUrl);
            using (var feedReader = new MyXmlReader(feedUrl))
            {
                var feedContent = SyndicationFeed.Load(feedReader);
                Rss20FeedFormatter rssFormatter = feedContent.GetRss20Formatter();
                

                if (null == feedContent)
                {
                    return list;
                }
                try
                {
                    foreach (var item in feedContent.Items)
                    {
                        String data = item.Title.Text.ToString();
                        if (check_data(data, date) == true)
                        {
                            list.Add(item.Summary.Text);
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
                catch (XmlException e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            return list;
        }

        public void pare_data(ArrayList list)
        {
            ArrayList result = new ArrayList();
            this._listResult = new Dictionary<string, ArrayList>();
            foreach (String i in list)
            {
                string[] res = i.Split(new string[] {"-", "1:", "2:", "3:", "4:", "5:", "6:", "7:", "8:", "ĐB:", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                long num;
                for (int j = 0; j < res.Length; j++)
                {
                    
                    if (!long.TryParse(res[j], out num))
                    {
                        if (!res[j].Trim().Equals(Constants.HCM, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.DT, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.BT, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.VT, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.CT, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.DN, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.AG, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.TN, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.BD, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.VL, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.LA, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.KG, StringComparison.InvariantCultureIgnoreCase) &&
                            !res[j].Trim().Equals(Constants.TG, StringComparison.InvariantCultureIgnoreCase))
                        {
                            j += 18;
                            continue;
                        }
                        result = new ArrayList();
                        if (!this._listResult.ContainsKey(res[j].Trim()))
                        {
                            this._listResult.Add(res[j].Trim(), result);
                        }
                    }
                    else
                    {
                        result.Add(res[j].Trim());
                    }
                }

                if (this._listResult.Count() == 0)
                {
                    this._listResult.Add("[Miền Bắc]", result);
                }
            }
        }

        public bool isFileOpen(string path_file)
        {
            FileStream fs = null;
            if (!File.Exists(path_file))
            {
                return false;
            }

            try
            {
                fs = new FileStream(path_file, FileMode.Open, FileAccess.Read, FileShare.None);
                return false;
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
    }
}
