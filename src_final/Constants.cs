﻿using System;
using System.Text;

namespace Lottery_Statistics
{
    static class Constants
    {

        public const String URL_MB = "http://xskt.com.vn/rss-feed/mien-bac-xsmb.rss";
        public const String URL_MN = "http://xskt.com.vn/rss-feed/mien-nam-xsmn.rss";

        /// <summary>
        /// Bao lô
        /// 
        /// </summary>
        public const String BL = "BL";

        /// <summary>
        /// Bao lô
        /// 
        /// </summary>
        public const String B = "B";

        /// <summary>
        /// Kéo Bao lô
        /// </summary>
        public const String KBL = "KBL";

        /// <summary>
        /// Đầu đuôi
        /// </summary>
        public const String DD = "DD";

        /// <summary>
        /// Đầu
        /// </summary>
        public const String DAU = "DAU";

        /// <summary>
        /// Đuôi
        /// </summary>
        public const String DUOI = "DUOI";

        /// <summary>
        /// Xì chủ
        /// </summary>
        public const String XC = "XC";

        /// <summary>
        /// Kéo Xì chủ
        /// </summary>
        public const String KXC = "KXC";

        /// <summary>
        /// Đảo bao lo
        /// </summary>
        public const String DBL = "DBL";

        /// <summary>
        /// Đảo xì chủ
        /// </summary>
        public const String DXC = "DXC";

        /// <summary>
        /// Đá
        /// </summary>
        public const String DA = "DA";

        /// <summary>
        /// Đá 2 dai
        /// </summary>
        public const String DX = "DX";

        /// <summary>
        /// Đá cap
        /// </summary>
        public const String DCA = "DCA";

        /// <summary>
        /// Đá vòng
        /// </summary>
        public const String DV = "DV";

        /// <summary>
        /// Miền nam
        /// </summary>
        public const String MN = "MN";

        /// <summary>
        /// Hai đài
        /// </summary>
        public const String HAIDAI = "2D";

        /// <summary>
        /// dai chinh
        /// </summary>
        public const String DC = "DC";

        /// <summary>
        /// dai phu
        /// </summary>
        public const String DP = "DP";

        /// <summary>
        /// 7 lo
        /// </summary>
        public const String SLO = "SLO";

        /// <summary>
        /// 7 lo display
        /// </summary>
        public const String SLOD = "7LO";

        /// <summary>
        /// Miền bắc
        /// </summary>
        public const String MB = "MB";

        /// <summary>
        /// Ăn tiền
        /// </summary>
        public const String WIN = "Thắng";

        /// <summary>
        /// Thứa cmnr
        /// </summary>
        public const String LOSE = "Thua";

        /// <summary>
        /// msg lỗi
        /// </summary>
        public const String ERROR = "Tin nhắn có lỗi";

        /// <summary>
        /// msg tinh choi khong co o ngay hien tai
        /// {0}: Tên đài không có trong ngày hiện tại
        /// </summary>
        public const String WRONG_PROVINCE = "Đài {0} không có kết quả số xố trong hôm nay";

        /// <summary>
        /// msg tinh choi khong co o ngay hien tai
        /// {0}: Số trúng
        /// {1}: Số lần trúng
        /// </summary>
        public const String FORMAT_RESULT = "{0}({1}*) - ";
        
        //Danh sách các đài
        /// <summary>
        /// Thứ 2
        /// </summary>
        public const String HCM = "[TP.HCM]";
        public const String DT = "[Đồng Tháp]";

        /// <summary>
        /// Thứ 3
        /// </summary>
        public const String BT = "[Bến Tre]";
        public const String VT = "[Vũng Tàu]";

        /// <summary>
        /// Thứ 4
        /// </summary>
        public const String CT = "[Cần Thơ]";
        public const String DN = "[Đồng Nai]";

        /// <summary>
        /// Thứ 5
        /// </summary>
        public const String AG = "[An Giang]";
        public const String TN = "[Tây Ninh]";

        /// <summary>
        /// Thứ 6
        /// </summary>
        public const String BD = "[Bình Dương]";
        public const String VL = "[Vĩnh Long]";

        /// <summary>
        /// Thứ 7
        /// </summary>
        //public const String HCM = "[TP.HCM]";
        public const String LA = "[Long An]";

        /// <summary>
        /// cn
        /// </summary>
        public const String KG = "[Kiên Giang]";
        public const String TG = "[Tiền Giang]";
    }
}
