﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Globalization;

namespace Lottery_Statistics
{
    class MyXmlReader : XmlTextReader
    {
        private bool readingDate = false;
        const string CustomUtcDateTimeFormat = "ddd MMM dd HH:mm:ss Z"; // Wed Oct 07 08:00:07 GMT 2009

        public MyXmlReader(Stream s) : base(s) { }
        public MyXmlReader(string inputUri) : base(inputUri) { }

        public override void ReadStartElement()
        {
            if (string.Equals(base.NamespaceURI, string.Empty, StringComparison.InvariantCultureIgnoreCase) &&
                (string.Equals(base.LocalName, "lastBuildDate", StringComparison.InvariantCultureIgnoreCase) ||
                string.Equals(base.LocalName, "pubDate", StringComparison.InvariantCultureIgnoreCase)))
            {
                readingDate = true;
            }
            base.ReadStartElement();
        }

        public override void ReadEndElement()
        {
            if (readingDate)
            {
                readingDate = false;
            }
            base.ReadEndElement();
        }

        public bool compare_date(DateTime parsedDate)
        {
            string[] dateValues = { "dd-MM-yyyy", "dd-MM-yyyy", 
                  "dd-MM-yy", "dd-MM-yy" };
            Boolean temp = false;
            foreach (var dateValue in dateValues)
            {
                if (DateTime.TryParseExact(dateValue, CustomUtcDateTimeFormat, null,
                                  DateTimeStyles.None, out parsedDate))
                {
                    temp = true;
                }
                else
                {
                    temp = false;
                }
            }
            return temp;
        }

        public override string ReadString()
        {
            if (readingDate)
            {
                string dateString = base.ReadString();
                DateTime dt = DateTime.Now.ToUniversalTime();
                if (compare_date(dt) == true)
                {
                    dt = DateTime.ParseExact(dateString,
                       CustomUtcDateTimeFormat,
                       CultureInfo.InvariantCulture,
                       DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);
                }
                return dt.ToString("R", CultureInfo.InvariantCulture);
            }
            else
            {
                return base.ReadString();
            }
        }
    }
}
