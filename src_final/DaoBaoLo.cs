﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lottery_Statistics
{
    class DaoBaoLo : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void tinh(Player thisPlayer, String digitVal, Double money, Region regionData1 = null, Region regionData2 = null)
        {
            IList<string> perms = Permutation_Generation.Permutation(digitVal);
            using (var bl = new BaoLo())
            {
                foreach (var perm in perms)
                {
                    bl.tinh(thisPlayer, perm, money, regionData1, regionData2);
                }
            }
        }
    }
}
