﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Lottery_Statistics
{
    class XiChu : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void tinh(Player thisPlayer, String digitVal, Double money, Region regionData1 = null, Region regionData2 = null)
        {

            ArrayList arrTmpDigit = new ArrayList();
            if (regionData1.arr3Digit.Count > 17)
            {
                //tính tổng tiền xác 3 digit
                thisPlayer.totalMoneyXac3Digit += money * 4;
                arrTmpDigit.Add(regionData1.arr3Digit[0]);
                arrTmpDigit.Add(regionData1.arr3Digit[20]);
                arrTmpDigit.Add(regionData1.arr3Digit[21]);
                arrTmpDigit.Add(regionData1.arr3Digit[22]);
            }
            else
            {
                //tính tổng tiền xác 3 digit
                thisPlayer.totalMoneyXac3Digit += money * 2;
                arrTmpDigit.Add(regionData1.arr3Digit[0]);
                arrTmpDigit.Add(regionData1.arr3Digit[16]);
                if (regionData2 != null)
                {
                    arrTmpDigit.Add(regionData2.arr3Digit[0]);
                    arrTmpDigit.Add(regionData1.arr3Digit[16]);
                }
            }

            var query = from string strtmp in arrTmpDigit
                        where strtmp.Equals(digitVal)
                        select strtmp;

            thisPlayer.totalMoneyWin3Digit += money * query.Count() * 650;
            if (query.Count() > 0)
            {
                thisPlayer.result += String.Format(Constants.FORMAT_RESULT, new object[] { digitVal, query.Count() });
                if (String.IsNullOrEmpty(thisPlayer.status) || !thisPlayer.status.Equals(Constants.WIN))
                {
                    thisPlayer.status = Constants.WIN;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(thisPlayer.status) || !thisPlayer.status.Equals(Constants.WIN))
                {
                    thisPlayer.status = Constants.LOSE;
                }
            }
        }
    }
}
