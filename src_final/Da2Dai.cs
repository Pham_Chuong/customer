﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Lottery_Statistics
{
    class Da2Dai : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void tinh(ArrayList arrDigit, Double money, List<Region> listregionData, Player thisPlayer)
        {
            List<ArrayList> arr2Digit;
            List<ArrayList> arr3Digit;
            switch (arrDigit[0].ToString().Length)
            {
                case 2:
                    arr2Digit = new List<ArrayList>();
                    arr2Digit.Add(listregionData[0].arr2Digit);
                    arr2Digit.Add(listregionData[1].arr2Digit);
                    thisPlayer.totalMoneyXac2Digit += money * (arr2Digit[0].Count + arr2Digit[1].Count) * arrDigit.Count;
                    processing4_DA_CM(arrDigit, money, arr2Digit, thisPlayer);
                    break;
                case 3:
                    arr3Digit = new List<ArrayList>();
                    arr3Digit.Add(listregionData[0].arr3Digit);
                    arr3Digit.Add(listregionData[1].arr3Digit);
                    thisPlayer.totalMoneyXac3Digit += money * (arr3Digit[0].Count + arr3Digit[1].Count) * arrDigit.Count;
                    processing4_DA_CM(arrDigit, money, arr3Digit, thisPlayer);
                    break;
            }
        }

        private void processing4_DA_CM(ArrayList arrDigit, Double money, List<ArrayList> regionData, Player thisPlayer)
        {
            List<ArrayList> listDa = new List<ArrayList>();
            switch (arrDigit.Count)
            {
                case 2:
                    listDa.Add(arrDigit);
                    break;
                default:
                    for (int i = 0; i < arrDigit.Count - 1; i++)
                    {
                        for (int j = i + 1; j < arrDigit.Count; j++)
                        {
                            ArrayList tmpDig = new ArrayList();
                            tmpDig.Add(arrDigit[i]);
                            tmpDig.Add(arrDigit[j]);
                            listDa.Add(tmpDig);
                        }
                    }
                    break;
            }
            foreach (var arr in listDa)
            {
                //tìm so choi trong mang 2 digit
                var query1_1 = from string strtmp in regionData[0]
                               where strtmp.Equals(arr[0])
                               select strtmp;
                var query1_2 = from string strtmp in regionData[1]
                               where strtmp.Equals(arr[0])
                               select strtmp;
                if ((query1_1.Count() + query1_1.Count())> 0)
                {
                    var query2_1 = from string strtmp in regionData[0]
                                   where strtmp.Equals(arr[1])
                                   select strtmp;
                    var query2_2 = from string strtmp in regionData[1]
                                     where strtmp.Equals(arr[1])
                                     select strtmp;
                    if ((query2_1.Count() + query2_2.Count()) > 0)
                    {
                        switch (arr[0].ToString().Length)
                        {
                            case 2:
                                //tính tổng tiền thắng của số 2 digit
                                thisPlayer.totalMoneyWin2Digit += money * arrDigit.Count * Math.Min((query1_1.Count() + query1_1.Count()), (query2_1.Count() + query2_2.Count()));
                                break;
                            case 3:
                                //tính tổng tiền thắng của số 3 digit
                                thisPlayer.totalMoneyWin3Digit += money * arrDigit.Count * Math.Min((query1_1.Count() + query1_1.Count()), (query2_1.Count() + query2_2.Count()));
                                break;
                        }
                        thisPlayer.result += arr[0] + "." + arr[1] + "(*)-";
                        if (String.IsNullOrEmpty(thisPlayer.status) || !thisPlayer.status.Equals(Constants.WIN))
                        {
                            thisPlayer.status = Constants.WIN;
                        }
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(thisPlayer.status) || !thisPlayer.status.Equals(Constants.WIN))
                        {
                            thisPlayer.status = Constants.LOSE;
                        }
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(thisPlayer.status) || !thisPlayer.status.Equals(Constants.WIN))
                    {
                        thisPlayer.status = Constants.LOSE;
                    }
                }
            }
        }
    }
}
