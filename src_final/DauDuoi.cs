﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Lottery_Statistics
{
    class DauDuoi : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public void tinh(Player thisPlayer, String digitVal, Double money, Region regionData1 = null, Region regionData2 = null, Boolean onlyDau = false, Boolean onlyDuoi = false)
        {
            ArrayList arrTmpDigit = new ArrayList();

            // chỉ chơi đầu
            if (onlyDau)
            {
                if (regionData1.arr2Digit.Count > 18)
                {

                    // miền bắc
                    thisPlayer.totalMoneyXac2Digit += money * 4;
                    arrTmpDigit.Add(regionData1.arr2Digit[23]);
                    arrTmpDigit.Add(regionData1.arr2Digit[24]);
                    arrTmpDigit.Add(regionData1.arr2Digit[25]);
                    arrTmpDigit.Add(regionData1.arr2Digit[26]);
                }
                else
                {
                    // miền nam
                    thisPlayer.totalMoneyXac2Digit += money * 1;
                    arrTmpDigit.Add(regionData1.arr2Digit[0]);
                    if (regionData2 != null)
                    {
                        arrTmpDigit.Add(regionData2.arr2Digit[0]);
                    }
                }                
            }

            // chỉ chơi đuôi
            if (onlyDuoi)
            {
                if (regionData1.arr2Digit.Count > 18)
                {

                    // miền bắc
                    thisPlayer.totalMoneyXac2Digit += money * 1;
                    arrTmpDigit.Add(regionData1.arr2Digit[0]);
                }
                else
                {
                    // miền nam
                    thisPlayer.totalMoneyXac2Digit += money * 1;
                    arrTmpDigit.Add(regionData1.arr2Digit[17]);
                    if (regionData2 != null)
                    {
                        arrTmpDigit.Add(regionData2.arr2Digit[17]);
                    }
                }
            }

            // cả đầu và đuôi
            if (!onlyDau && !onlyDuoi)
            {
                if (regionData1.arr2Digit.Count > 18)
                {
                    // miền bắc
                    thisPlayer.totalMoneyXac2Digit += money * 5;
                    arrTmpDigit.Add(regionData1.arr2Digit[0]);
                    arrTmpDigit.Add(regionData1.arr2Digit[23]);
                    arrTmpDigit.Add(regionData1.arr2Digit[24]);
                    arrTmpDigit.Add(regionData1.arr2Digit[25]);
                    arrTmpDigit.Add(regionData1.arr2Digit[26]);
                }
                else
                {
                    // miền nam
                    thisPlayer.totalMoneyXac2Digit += money * 2;
                    arrTmpDigit.Add(regionData1.arr2Digit[0]);
                    arrTmpDigit.Add(regionData1.arr2Digit[17]);
                    if (regionData2 != null)
                    {
                        arrTmpDigit.Add(regionData2.arr2Digit[0]);
                        arrTmpDigit.Add(regionData2.arr2Digit[17]);
                    }
                }
            }
            
            //tim so lan xuat hien cua strtmp trong arrTmpDigit
            var query = from string strtmp in arrTmpDigit
                          where strtmp.Equals(digitVal)
                          select strtmp;

            thisPlayer.totalMoneyWin2Digit += money * query.Count() * 75;
            if (query.Count() > 0)
            {
                thisPlayer.result += String.Format(Constants.FORMAT_RESULT, new object[] {digitVal, query.Count()});
                if (String.IsNullOrEmpty(thisPlayer.status) || !thisPlayer.status.Equals(Constants.WIN))
                {
                    thisPlayer.status = Constants.WIN;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(thisPlayer.status) || !thisPlayer.status.Equals(Constants.WIN))
                {
                    thisPlayer.status = Constants.LOSE;
                }
            }
        }
    }
}
