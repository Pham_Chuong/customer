﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Lottery_Statistics
{
    class Region
    {
        private ArrayList _arr2Digit;
        public ArrayList arr2Digit
        {
            get { return _arr2Digit; }
            set { _arr2Digit = value; }
        }

        private ArrayList _arr3Digit;
        public ArrayList arr3Digit
        {
            get { return _arr3Digit; }
            set { _arr3Digit = value; }
        }

        private ArrayList _arr4Digit;
        public ArrayList arr4Digit
        {
            get { return _arr4Digit; }
            set { _arr4Digit = value; }
        }
    }
}
