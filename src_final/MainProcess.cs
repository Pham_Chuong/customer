﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel.Syndication;
using System.Diagnostics;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Globalization;

namespace Lottery_Statistics
{
    class MainProcess
    {
        public static string pathFile = String.Empty;
        public SubProcess subProcess = new SubProcess();

        private static Regex regexTP = new Regex(@"\D+");
        private static Regex regexNM = new Regex(@"\d+^,\d+");

        private Region regionMB;
        private Region regionMN_DC;
        private Region regionMN_DP;
        private Dictionary<string, string> dicDaiChinh = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            {"HCM", Constants.HCM},            
            {"BT", Constants.BT},
            {"CT", Constants.CT},
            {"AG", Constants.AG},
            {"BD", Constants.BD},
            {"KG", Constants.KG},
            {"DC", Constants.DC}
        };

        private Dictionary<string, string> dicDaiPhu = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            {"DT", Constants.DT},
            {"VT", Constants.VT},
            {"DN", Constants.DN},
            {"TN", Constants.TN},
            {"VL", Constants.VL},
            {"LA", Constants.LA},
            {"TG", Constants.TG},
            {"DP", Constants.DP}
        };

        /// <summary>
        /// chuan bi truoc khi tinh toan
        /// </summary>
        /// <param name="feedUrl"></param>
        /// <returns></returns>
        private string prepare4Calculate(String feedUrl)
        {
            String strMsg = String.Empty;

            //Check connection
            strMsg = this.subProcess.checkConnection();
            if (!String.IsNullOrEmpty(strMsg))
            {
                return strMsg;
            }

            //check input path file
            if (String.IsNullOrEmpty(pathFile))
            {
                strMsg = "Hãy chọn file trước khi thực hiện tính toán!!!";
                return strMsg;
            }

            // read file txt
            this.subProcess.readFile(pathFile);

            //var date = DateTime.Now.ToString("dd/MM");
            var date = "06/12";
            if (this.subProcess.get_data_rss(feedUrl, date).Count == 0)
            {
                strMsg = "Hiện tại chưa có kết quả sổ xố hôm nay!!!";
                return strMsg;
            }

            this.subProcess.pare_data(subProcess.get_data_rss(feedUrl, date));
            return strMsg;
        }

        /// <summary>
        /// get array digit theo tung vung
        /// </summary>
        /// <param name="region"></param>
        private void getArrDigit(String region)
        {
            this.regionMB = new Region();
            this.regionMN_DC = new Region();
            this.regionMN_DP = new Region();

            ArrayList arrTmp2Digit;
            ArrayList arrTmp3Digit;
            ArrayList arrTmp4Digit;

            foreach (KeyValuePair<string, ArrayList> pair in subProcess.listResult)
            {
                arrTmp2Digit = new ArrayList();
                arrTmp3Digit = new ArrayList();
                arrTmp4Digit = new ArrayList();
                foreach (var strTmp in pair.Value)
                {
                    arrTmp2Digit.Add(strTmp.ToString().Substring(strTmp.ToString().Length - 2, 2));
                    if (strTmp.ToString().Length >= 3)
                    {
                        arrTmp3Digit.Add(strTmp.ToString().Substring(strTmp.ToString().Length - 3, 3));
                    }

                    if (strTmp.ToString().Length >= 4)
                    {
                        arrTmp4Digit.Add(strTmp.ToString().Substring(strTmp.ToString().Length - 4, 4));
                    }
                }

                switch (region)
                {
                    case Constants.MB:
                        this.regionMB.arr2Digit = arrTmp2Digit;
                        this.regionMB.arr3Digit = arrTmp3Digit;
                        this.regionMB.arr4Digit = arrTmp4Digit;
                        break;
                    case Constants.MN:
                        if (dicDaiChinh.ContainsValue(pair.Key))
                        {
                            this.regionMN_DC.arr2Digit = arrTmp2Digit;
                            this.regionMN_DC.arr3Digit = arrTmp3Digit;
                            this.regionMN_DC.arr4Digit = arrTmp4Digit;
                        }
                        else
                        {
                            this.regionMN_DP.arr2Digit = arrTmp2Digit;
                            this.regionMN_DP.arr3Digit = arrTmp3Digit;
                            this.regionMN_DP.arr4Digit = arrTmp4Digit;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Tinh toan
        /// </summary>
        /// <param name="region"></param>
        /// <returns></returns>
        public String Calculate(String region)
        {
            String strMsg = String.Empty;
            String feedUrl = String.Empty;
            if (Equals(Constants.MN, region))
            {
                feedUrl = Constants.URL_MN;
            }
            else
            {
                feedUrl = Constants.URL_MB;
            }

            strMsg = this.prepare4Calculate(feedUrl);
            if (strMsg.Length != 0)
            {
                return strMsg;
            }

            getArrDigit(region);

            // loop từng người chơi
            foreach (KeyValuePair<Int32, Player> pair in subProcess.listPlayer)
            {
                this.processing(pair.Value, region);
            }
            return strMsg;
        }

        /// <summary>
        /// Main process
        /// </summary>
        /// <param name="strMsg"></param>
        /// <param name="digit"></param>
        private void processing(Player thisPlayer, String region)
        {
            if (!thisPlayer.strMsg.Contains("."))
            {
                thisPlayer.status = Constants.ERROR;
                return;
            }

            String strPattern = String.Empty;
            String daiMN = String.Empty;
            String strMsgAfter = thisPlayer.strMsg;
            String[] arrParseMsg;
            ArrayList arrDigit = new ArrayList();
            ArrayList arrTypePlay = new ArrayList();
            Boolean blnCheckGetSuccessfull = false;

            // cắt khoảng trắng đầu cuối
            strMsgAfter = strMsgAfter.TrimStart().TrimEnd();

            // cắt dấu chấm hoặc dấu phẩy nếu có
            strMsgAfter = strMsgAfter.TrimStart('.').TrimEnd('.');
            strMsgAfter = strMsgAfter.TrimStart(',').TrimEnd(',');

            // pattern cho trường hợp kéo bao lo và kéo xì chủ
            strPattern = @"^(\D{2,4}\.)?\d{3}\.(KBL|KXC).\d{3}\.\d+(,\d+)?n?$";
            if (Regex.IsMatch(strMsgAfter, strPattern, RegexOptions.IgnoreCase))
            {
                blnCheckGetSuccessfull = true;
                // tách tin nhắn theo dấu chấm
                arrParseMsg = strMsgAfter.Split('.');

                // Get phần tử đầu tiên trong tin nhắn trong trường hợp chơi 1 đài
                if (region.Equals(Constants.MN))
                {
                    if (!arrParseMsg[0].All(char.IsDigit))
                    {
                        daiMN = arrParseMsg[0];
                        arrParseMsg = arrParseMsg.Where(w => w != arrParseMsg[0]).ToArray();
                    }
                }
                else
                {
                    if (!arrParseMsg[0].All(char.IsDigit))
                    {
                        thisPlayer.status = Constants.ERROR;
                        return;
                    }
                }

                // convert string thành số
                int soStart = 0;
                int soEnd = 0;
                int soKeo = 0;
                try
                {
                    soStart = Int32.Parse(arrParseMsg[0]);
                    soEnd = Int32.Parse(arrParseMsg[2]);
                    soKeo = getSoKeo(arrParseMsg[0], arrParseMsg[2]);
                    if (soKeo == -1) 
                    {
                        throw new Exception();
                    }
                    //set arrDigit
                    while (soStart <= soEnd)
                    {
                        if (soStart.ToString().Length == 2)
                        {
                            arrDigit.Add('0' + soStart.ToString());
                        }
                        else
                        {
                            arrDigit.Add(soStart.ToString());
                        }
                        
                        soStart += soKeo;
                    }

                    //Set arrTypePlay
                    arrTypePlay.Add(arrParseMsg[1] + arrParseMsg[3]);
                }
                catch (Exception)
                {
                    thisPlayer.status = Constants.ERROR;
                    return;
                }
            }

            // Pattern cho truong hop đá cặp
            strPattern = @"^(\D{2,4}\.)?(\d{2,4}\.\d{2,4}\s)+\d{2,4}\.\d{2,4}\.DA\d+(,\d+)?n?$";
            if (Regex.IsMatch(strMsgAfter, strPattern, RegexOptions.IgnoreCase))
            {
                blnCheckGetSuccessfull = false;
                // change typeplay to DC
                strMsgAfter = Regex.Replace(strMsgAfter, "DA", "DCA", RegexOptions.IgnoreCase);
                // tách tin nhắn theo dấu space
                strMsgAfter = Regex.Replace(strMsgAfter, " ", ".", RegexOptions.IgnoreCase);
                arrParseMsg = strMsgAfter.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
            }

            // Pattern trường hợp dau duoi
            strPattern = @"^(\D{2,4}\.)?(\d{2,4}\.)+(DAU\d+(,\d+)?n?.)?(DUOI\d+(,\d+)?n?)?$";
            if (Regex.IsMatch(strMsgAfter, strPattern, RegexOptions.IgnoreCase))
            {
                blnCheckGetSuccessfull = true;
                // tách tin nhắn theo dấu chấm
                arrParseMsg = strMsgAfter.Split('.');

                // Get phần tử đầu tiên trong tin nhắn trong trường hợp chơi 1 đài
                if (region.Equals(Constants.MN))
                {
                    if (!arrParseMsg[0].All(char.IsDigit))
                    {
                        daiMN = Constants.HAIDAI.Equals(arrParseMsg[0], StringComparison.InvariantCultureIgnoreCase) ? String.Empty : arrParseMsg[0];
                        arrParseMsg = arrParseMsg.Where(w => w != arrParseMsg[0]).ToArray();
                    }
                }
                else
                {
                    if (!arrParseMsg[0].All(char.IsDigit))
                    {
                        thisPlayer.status = Constants.ERROR;
                        return;
                    }
                }

                // loop từng số
                for (int i = 0; i < arrParseMsg.Count(); i++)
                {
                    if (arrParseMsg[i].All(char.IsDigit))
                    {
                        arrDigit.Add(arrParseMsg[i]);
                    }
                    else
                    {
                        arrTypePlay.Add(arrParseMsg[i]);
                    }
                }
            }

            // Pattern trường hợp dau duoi
            strPattern = @"^(\D{2,4}\.)?\d{4}\.BL\d+(,\d+)?n?\.BL\d+(,\d+)?n?";
            if (Regex.IsMatch(strMsgAfter, strPattern, RegexOptions.IgnoreCase))
            {
                blnCheckGetSuccessfull = true;
                // tách tin nhắn theo dấu chấm
                
                arrParseMsg = strMsgAfter.Split('.');

                // Get phần tử đầu tiên trong tin nhắn trong trường hợp chơi 1 đài
                if (region.Equals(Constants.MN))
                {
                    if (!arrParseMsg[0].All(char.IsDigit))
                    {
                        daiMN = Constants.HAIDAI.Equals(arrParseMsg[0], StringComparison.InvariantCultureIgnoreCase) ? String.Empty : arrParseMsg[0];
                        arrParseMsg = arrParseMsg.Where(w => w != arrParseMsg[0]).ToArray();
                    }
                }
                else
                {
                    if (!arrParseMsg[0].All(char.IsDigit))
                    {
                        thisPlayer.status = Constants.ERROR;
                        return;
                    }
                }

                if (arrParseMsg[0].Length != 4)
                {
                    thisPlayer.status = Constants.ERROR;
                    return;
                }

                // loop từng số
                for (int i = 0; i < arrParseMsg.Count(); i++)
                {
                    if (arrParseMsg[i].All(char.IsDigit))
                    {
                        arrDigit.Add(arrParseMsg[i]);
                    }
                    else
                    {
                        arrTypePlay.Add(arrParseMsg[i]);
                    }
                }

                tinhBaoLo(thisPlayer, arrDigit[0].ToString(), arrTypePlay[0].ToString(), region, daiMN);
                arrDigit = new ArrayList();
                arrDigit.Add(arrParseMsg[0].Remove(0, 1));
                arrTypePlay.Remove(0);
            }

            // Pattern trường hợp dau duoi
            strPattern = @"^(\D{2,4}\.)?\d{3}\.7LO.\d+(,\d+)?n?";
            if (Regex.IsMatch(strMsgAfter, strPattern, RegexOptions.IgnoreCase))
            {
                blnCheckGetSuccessfull = true;
                // tách tin nhắn theo dấu chấm
                strMsgAfter = Regex.Replace(strMsgAfter, "7LO", "SLO", RegexOptions.IgnoreCase);
                arrParseMsg = strMsgAfter.Split('.');

                // Get phần tử đầu tiên trong tin nhắn trong trường hợp chơi 1 đài
                if (region.Equals(Constants.MN))
                {
                    if (!arrParseMsg[0].All(char.IsDigit))
                    {
                        daiMN = Constants.HAIDAI.Equals(arrParseMsg[0], StringComparison.InvariantCultureIgnoreCase) ? String.Empty : arrParseMsg[0];
                        arrParseMsg = arrParseMsg.Where(w => w != arrParseMsg[0]).ToArray();
                    }
                }
                else
                {
                    if (!arrParseMsg[0].All(char.IsDigit))
                    {
                        thisPlayer.status = Constants.ERROR;
                        return;
                    }
                }

                arrDigit.Add(arrParseMsg[0]);
                arrTypePlay.Add(arrParseMsg[1] + arrParseMsg[2]);
            }

            if (!blnCheckGetSuccessfull)
            {
                Boolean startdigit = false;
                Boolean enddigit = false;
                //tách số đánh vs cách chơi
                arrParseMsg = strMsgAfter.Split(new string[] {"."}, StringSplitOptions.RemoveEmptyEntries);

                // Get phần tử đầu tiên trong tin nhắn trong trường hợp chơi 1 đài
                if (region.Equals(Constants.MN))
                {
                    if (!arrParseMsg[0].All(char.IsDigit))
                    {
                        daiMN = Constants.HAIDAI.Equals(arrParseMsg[0], StringComparison.InvariantCultureIgnoreCase) ? String.Empty : arrParseMsg[0];
                        arrParseMsg = arrParseMsg.Where(w => w != arrParseMsg[0]).ToArray();
                    }
                }

                foreach (var tmp in arrParseMsg)
                {
                    if (tmp.All(char.IsDigit))
                    {
                        // if con số chơi < 2 > 4 
                        if (tmp.Length < 2 || tmp.Length > 4)
                        {
                            thisPlayer.status = Constants.ERROR;
                            return;
                        }

                        if (enddigit)
                        {
                            thisPlayer.status = Constants.ERROR;
                            return;
                        }

                        startdigit = true;
                        //get digit
                        arrDigit.Add(tmp);
                    }
                    else
                    {
                        if (!startdigit)
                        {
                            thisPlayer.status = Constants.ERROR;
                            return;
                        }

                        if (tmp.Length < 3)
                        {
                            thisPlayer.status = Constants.ERROR;
                            return;
                        }
                        enddigit = true;
                        //get type play
                        arrTypePlay.Add(tmp);
                    }
                }
            }

            //lặp số cách chơi
            foreach (var tmp in arrTypePlay)
            {
                Double money = getMoney(tmp.ToString());
                if (money == 0.0)
                {
                    clearPlayer(thisPlayer);
                    return;
                }

                switch (regexTP.Match(tmp.ToString()).Value.ToUpper())
                {
                    case Constants.BL:
                        using (var bl = new BaoLo())
                        {
                            foreach (var tmpDigit in arrDigit)
                            {
                                // neu nho hon 2 digit hoac lon hon 4 digit thi bao loi
                                if (tmpDigit.ToString().Length < 2 || tmpDigit.ToString().Length > 4)
                                {
                                    clearPlayer(thisPlayer);
                                    return;
                                }

                                if (region.Equals(Constants.MB))
                                {
                                    bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMB);
                                }
                                else if (!String.IsNullOrEmpty(daiMN))
                                {
                                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                    {
                                        clearPlayer(thisPlayer);
                                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                        return;
                                    }

                                    if (this.dicDaiChinh.ContainsKey(daiMN))
                                    {
                                        bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC);
                                    }
                                    else
                                    {
                                        bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DP);
                                    }
                                }
                                else
                                {
                                    bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, this.regionMN_DP);
                                }

                                if (thisPlayer.result.Contains('*'))
                                {
                                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.BL);
                                }
                            }
                        }
                        break;
                    case Constants.B:
                        using (var bl = new BaoLo())
                        {
                            foreach (var tmpDigit in arrDigit)
                            {
                                // neu nho hon 2 digit hoac lon hon 4 digit thi bao loi
                                if (tmpDigit.ToString().Length < 2 || tmpDigit.ToString().Length > 4)
                                {
                                    clearPlayer(thisPlayer);
                                    return;
                                }

                                if (region.Equals(Constants.MB))
                                {
                                    bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMB);
                                }
                                else if (!String.IsNullOrEmpty(daiMN))
                                {
                                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                    {
                                        clearPlayer(thisPlayer);
                                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                        return;
                                    }

                                    if (this.dicDaiChinh.ContainsKey(daiMN))
                                    {
                                        bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC);
                                    }
                                    else
                                    {
                                        bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DP);
                                    }
                                }
                                else
                                {
                                    bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, this.regionMN_DP);
                                }

                                if (thisPlayer.result.Contains('*'))
                                {
                                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.BL);
                                }
                            }
                        }
                        break;
                    case Constants.DBL:
                        using (var dbl = new DaoBaoLo())
                        {
                            foreach (var tmpDigit in arrDigit)
                            {
                                // nếu nho hon 2 digit or lon hon 4 thi bao loi
                                if (tmpDigit.ToString().Length < 2 || tmpDigit.ToString().Length > 4)
                                {
                                    clearPlayer(thisPlayer);
                                    return;
                                }

                                if (region.Equals(Constants.MB))
                                {
                                    dbl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMB);
                                }
                                else if (!String.IsNullOrEmpty(daiMN))
                                {
                                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                    {
                                        clearPlayer(thisPlayer);
                                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                        return;
                                    }

                                    if (this.dicDaiChinh.ContainsKey(daiMN))
                                    {
                                        dbl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC);
                                    }
                                    else
                                    {
                                        dbl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DP);
                                    }
                                }
                                else
                                {
                                    dbl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, this.regionMN_DP);
                                }
                                if (thisPlayer.result.Contains('*'))
                                {
                                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.DBL);
                                }
                            }
                        }
                        break;
                    case Constants.KBL:
                        using (var bl = new BaoLo())
                        {
                            foreach (var tmpDigit in arrDigit)
                            {
                                // neu nho hon 2 digit hoac lon hon 4 digit thi bao loi
                                if (tmpDigit.ToString().Length < 2 || tmpDigit.ToString().Length > 4)
                                {
                                    clearPlayer(thisPlayer);
                                    return;
                                }

                                if (region.Equals(Constants.MB))
                                {
                                    bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMB);
                                }
                                else if (!String.IsNullOrEmpty(daiMN))
                                {
                                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                    {
                                        clearPlayer(thisPlayer);
                                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                        return;
                                    }

                                    if (this.dicDaiChinh.ContainsKey(daiMN))
                                    {
                                        bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC);
                                    }
                                    else
                                    {
                                        bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DP);
                                    }
                                }
                                else
                                {
                                    bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, this.regionMN_DP);
                                }

                                if (thisPlayer.result.Contains('*'))
                                {
                                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.KBL);
                                }
                            }
                        }
                        break;
                    case Constants.DA:
                        //it hon 2 con thi co loi
                        if (arrDigit.Count < 2)
                        {
                            clearPlayer(thisPlayer);
                            return;
                        }

                        //các con số đánh không cùng là 2 con hoac 3 con
                        for (int i = 0; i < arrDigit.Count - 1; i++)
                        {
                            // nếu các số chơi ít hơn 2 hoặc lớn hơn 3 digit thì có lỗi
                            if (arrDigit[i].ToString().Length < 2 || arrDigit[i].ToString().Length > 3)
                            {
                                clearPlayer(thisPlayer);
                                return;
                            }

                            // số đầu tien và những số còn lại có số chữ số khác nhau la lỗi
                            if (arrDigit[0].ToString().Length != arrDigit[i].ToString().Length)
                            {
                                clearPlayer(thisPlayer);
                                return;
                            }
                        }

                        using (var da = new Da())
                        {
                            if (region.Equals(Constants.MB))
                            {
                                da.tinh(thisPlayer, arrDigit, money, this.regionMB);
                            }
                            else if (!String.IsNullOrEmpty(daiMN))
                            {
                                if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                {
                                    clearPlayer(thisPlayer);
                                    thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                    return;
                                }

                                if (this.dicDaiChinh.ContainsKey(daiMN))
                                {
                                    da.tinh(thisPlayer, arrDigit, money, this.regionMN_DC);
                                }
                                else
                                {
                                    da.tinh(thisPlayer, arrDigit, money, this.regionMN_DP);
                                }
                            }
                            else
                            {
                                clearPlayer(thisPlayer);
                                return;
                            }
                            if (thisPlayer.result.Contains('*'))
                            {
                                thisPlayer.result = thisPlayer.result.Replace("*", Constants.DA);
                            }
                        }
                        break;
                    case Constants.DX:

                        if (region.Equals(Constants.MB))
                        {
                            clearPlayer(thisPlayer);
                            return;
                        }

                        //it hon 2 con thi co loi
                        if (arrDigit.Count < 2)
                        {
                            clearPlayer(thisPlayer);
                            return;
                        }

                        //các con số đánh không cùng là 2 con hoac 3 con
                        for (int i = 0; i < arrDigit.Count - 1; i++)
                        {
                            // nếu các số chơi ít hơn 2 hoặc lớn hơn 3 digit thì có lỗi
                            if (arrDigit[i].ToString().Length < 2 || arrDigit[i].ToString().Length > 3)
                            {
                                clearPlayer(thisPlayer);
                                return;
                            }

                            // số đầu tien và những số còn lại có số chữ số khác nhau la lỗi
                            if (arrDigit[0].ToString().Length != arrDigit[i].ToString().Length)
                            {
                                clearPlayer(thisPlayer);
                                return;
                            }
                        }

                        using (var da2dai = new Da())
                        {
                            da2dai.tinh(thisPlayer, arrDigit, money, this.regionMN_DC, this.regionMN_DP);
                            if (thisPlayer.result.Contains('*'))
                            {
                                thisPlayer.result = thisPlayer.result.Replace("*", Constants.DX);
                            }
                        }
                        break;
                    case Constants.DCA:
                        //it hon 2 con thi co loi
                        if (arrDigit.Count % 2 != 0)
                        {
                            clearPlayer(thisPlayer);
                            return;
                        }

                        //các con số đánh không cùng là 2 con hoac 3 con
                        for (int i = 0; i < arrDigit.Count - 1; i+=2)
                        {
                            // nếu các số chơi ít hơn 2 hoặc lớn hơn 3 digit thì có lỗi
                            if (arrDigit[i].ToString().Length < 2 || arrDigit[i].ToString().Length > 3)
                            {
                                clearPlayer(thisPlayer);
                                return;
                            }

                            // số đầu tien và những số còn lại có số chữ số khác nhau la lỗi
                            if (arrDigit[i].ToString().Length != arrDigit[i+1].ToString().Length)
                            {
                                clearPlayer(thisPlayer);
                                return;
                            }
                        }

                        using (var da = new Da())
                        {
                            if (region.Equals(Constants.MB))
                            {
                                da.tinh(thisPlayer, arrDigit, money, this.regionMB, isDaCap:true);
                            }
                            else if (!String.IsNullOrEmpty(daiMN))
                            {
                                if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                {
                                    clearPlayer(thisPlayer);
                                    thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                    return;
                                }

                                if (this.dicDaiChinh.ContainsKey(daiMN))
                                {
                                    da.tinh(thisPlayer, arrDigit, money, this.regionMN_DC, isDaCap: true);
                                }
                                else
                                {
                                    da.tinh(thisPlayer, arrDigit, money, this.regionMN_DP, isDaCap: true);
                                }
                            }
                            else
                            {
                                da.tinh(thisPlayer, arrDigit, money, this.regionMN_DC, this.regionMN_DP, isDaCap: true);
                            }
                            if (thisPlayer.result.Contains('*'))
                            {
                                thisPlayer.result = thisPlayer.result.Replace("*", Constants.DA);
                            }
                        }
                        break;
                    case Constants.DV:

                        //it hon 2 con thi co loi
                        if (arrDigit.Count < 2)
                        {
                            clearPlayer(thisPlayer);
                            return;
                        }

                        //các con số đánh không cùng là 2 con hoac 3 con
                        for (int i = 0; i < arrDigit.Count - 1; i++)
                        {
                            // nếu các số chơi ít hơn 2 hoặc lớn hơn 3 digit thì có lỗi
                            if (arrDigit[i].ToString().Length < 2 || arrDigit[i].ToString().Length > 3)
                            {
                                clearPlayer(thisPlayer);
                                return;
                            }

                            // số đầu tien và những số còn lại có số chữ số khác nhau la lỗi
                            if (arrDigit[0].ToString().Length != arrDigit[i].ToString().Length)
                            {
                                clearPlayer(thisPlayer);
                                return;
                            }
                        }

                        using (var da = new Da())
                        {
                            if (region.Equals(Constants.MB))
                            {
                                da.tinh(thisPlayer, arrDigit, money, this.regionMB);
                            }
                            else if (!String.IsNullOrEmpty(daiMN))
                            {
                                if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                {
                                    clearPlayer(thisPlayer);
                                    thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                    return;
                                }

                                if (this.dicDaiChinh.ContainsKey(daiMN))
                                {
                                    da.tinh(thisPlayer, arrDigit, money, this.regionMN_DC);
                                }
                                else
                                {
                                    da.tinh(thisPlayer, arrDigit, money, this.regionMN_DP);
                                }
                            }
                            else
                            {
                                da.tinh(thisPlayer, arrDigit, money, this.regionMN_DC, this.regionMN_DP);
                            }
                            if (thisPlayer.result.Contains('*'))
                            {
                                thisPlayer.result = thisPlayer.result.Replace("*", Constants.DV);
                            }
                        }
                        break;
                    case Constants.DD:
                        using (var dd = new DauDuoi())
                        {
                            foreach (var tmpDigit in arrDigit)
                            {
                                // nếu không phải là 2 digit thi co loi
                                if (tmpDigit.ToString().Length != 2)
                                {
                                    clearPlayer(thisPlayer);
                                    return;
                                }

                                if (region.Equals(Constants.MB))
                                {
                                    dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMB);

                                }
                                else if (!String.IsNullOrEmpty(daiMN))
                                {
                                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                    {
                                        clearPlayer(thisPlayer);
                                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                        return;
                                    }

                                    if (this.dicDaiChinh.ContainsKey(daiMN))
                                    {
                                        dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC);
                                    }
                                    else
                                    {
                                        dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DP);
                                    }
                                }
                                else
                                {
                                    dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, this.regionMN_DP);
                                }
                                if (thisPlayer.result.Contains('*'))
                                {
                                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.DD);
                                }
                            }
                        }
                        break;
                    case Constants.XC:
                        using (var xc = new XiChu())
                        {
                            foreach (var tmpDigit in arrDigit)
                            {
                                // nếu không phải là 3 digit thi co loi
                                if (tmpDigit.ToString().Length != 3)
                                {
                                    clearPlayer(thisPlayer);
                                    return;
                                }

                                if (region.Equals(Constants.MB))
                                {
                                    xc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMB);
                                }
                                else if (!String.IsNullOrEmpty(daiMN))
                                {
                                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                    {
                                        clearPlayer(thisPlayer);
                                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                        return;
                                    }

                                    if (this.dicDaiChinh.ContainsKey(daiMN))
                                    {
                                        xc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC);
                                    }
                                    else
                                    {
                                        xc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DP);
                                    }
                                }
                                else
                                {
                                    xc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, this.regionMN_DP);
                                }
                                if (thisPlayer.result.Contains('*'))
                                {
                                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.XC);
                                }
                            }
                        }
                        break;
                    case Constants.DXC:
                        using (var dxc = new DaoXiChu())
                        {
                            foreach (var tmpDigit in arrDigit)
                            {
                                // nếu không phải là 3 digit thi co loi
                                if (tmpDigit.ToString().Length != 3)
                                {
                                    clearPlayer(thisPlayer);
                                    return;
                                }

                                if (region.Equals(Constants.MB))
                                {
                                    dxc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMB);
                                }
                                else if (!String.IsNullOrEmpty(daiMN))
                                {
                                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                    {
                                        clearPlayer(thisPlayer);
                                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                        return;
                                    }

                                    if (this.dicDaiChinh.ContainsKey(daiMN))
                                    {
                                        dxc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC);
                                    }
                                    else
                                    {
                                        dxc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DP);
                                    }
                                }
                                else
                                {
                                    dxc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, this.regionMN_DP);
                                }
                                if (thisPlayer.result.Contains('*'))
                                {
                                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.XC);
                                }
                            }
                        }
                        break;
                    case Constants.KXC:
                        using (var xc = new XiChu())
                        {
                            foreach (var tmpDigit in arrDigit)
                            {
                                // nếu không phải là 3 digit thi co loi
                                if (tmpDigit.ToString().Length != 3)
                                {
                                    clearPlayer(thisPlayer);
                                    return;
                                }

                                if (region.Equals(Constants.MB))
                                {
                                    xc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMB);
                                }
                                else if (!String.IsNullOrEmpty(daiMN))
                                {
                                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                    {
                                        clearPlayer(thisPlayer);
                                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                        return;
                                    }

                                    if (this.dicDaiChinh.ContainsKey(daiMN))
                                    {
                                        xc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC);
                                    }
                                    else
                                    {
                                        xc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DP);
                                    }
                                }
                                else
                                {
                                    xc.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, this.regionMN_DP);
                                }
                                if (thisPlayer.result.Contains('*'))
                                {
                                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.KXC);
                                }
                            }
                        }
                        break;
                    case Constants.DAU:
                        using (var dd = new DauDuoi())
                        {
                            foreach (var tmpDigit in arrDigit)
                            {
                                // nếu không phải là 2 digit thì có lỗi
                                if (tmpDigit.ToString().Length != 2)
                                {
                                    clearPlayer(thisPlayer);
                                    return;
                                }

                                if (region.Equals(Constants.MB))
                                {
                                    dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMB, onlyDau: true);
                                }
                                else if (!String.IsNullOrEmpty(daiMN))
                                {
                                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                    {
                                        clearPlayer(thisPlayer);
                                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                        return;
                                    }

                                    if (this.dicDaiChinh.ContainsKey(daiMN))
                                    {
                                        dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, onlyDau: true);
                                    }
                                    else
                                    {
                                        dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DP, onlyDau: true);
                                    }
                                }
                                else
                                {
                                    dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, this.regionMN_DP, onlyDau: true);
                                }
                                if (thisPlayer.result.Contains('*'))
                                {
                                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.DAU);
                                }
                            }
                        }
                        break;
                    case Constants.DUOI:
                        using (var dd = new DauDuoi())
                        {
                            foreach (var tmpDigit in arrDigit)
                            {
                                // nếu không phải là 2 digit thì có lỗi
                                if (tmpDigit.ToString().Length != 2)
                                {
                                    clearPlayer(thisPlayer);
                                    return;
                                }

                                if (region.Equals(Constants.MB))
                                {
                                    dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMB, onlyDuoi: true);
                                }
                                else if (!String.IsNullOrEmpty(daiMN))
                                {
                                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                    {
                                        clearPlayer(thisPlayer);
                                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                        return;
                                    }

                                    if (this.dicDaiChinh.ContainsKey(daiMN))
                                    {
                                        dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, onlyDuoi: true);
                                    }
                                    else
                                    {
                                        dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DP, onlyDuoi: true);
                                    }
                                }
                                else
                                {
                                    dd.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, this.regionMN_DP, onlyDuoi: true);
                                }
                                if (thisPlayer.result.Contains('*'))
                                {
                                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.DUOI);
                                }
                            }
                        }
                        break;
                    case Constants.SLO:
                        using (var bl = new BaoLo())
                        {
                            foreach (var tmpDigit in arrDigit)
                            {
                                // neu nho hon 2 digit hoac lon hon 4 digit thi bao loi
                                if (tmpDigit.ToString().Length < 2 || tmpDigit.ToString().Length > 4)
                                {
                                    clearPlayer(thisPlayer);
                                    return;
                                }

                                if (region.Equals(Constants.MB))
                                {
                                    bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMB, is7Lo: true);
                                }
                                else if (!String.IsNullOrEmpty(daiMN))
                                {
                                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                                    {
                                        clearPlayer(thisPlayer);
                                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                                        return;
                                    }

                                    if (this.dicDaiChinh.ContainsKey(daiMN))
                                    {
                                        bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, is7Lo: true);
                                    }
                                    else
                                    {
                                        bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DP, is7Lo: true);
                                    }
                                }
                                else
                                {
                                    bl.tinh(thisPlayer, tmpDigit.ToString(), money, this.regionMN_DC, this.regionMN_DP, is7Lo: true);
                                }

                                if (thisPlayer.result.Contains('*'))
                                {
                                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.SLOD);
                                }
                            }
                        }
                        break;
                    default:
                        clearPlayer(thisPlayer);
                        return;
                }
            }
        }

        private void tinhBaoLo(Player thisPlayer, String strNum, String typePlay, String region, String daiMN = null)
        {
            using (var bl = new BaoLo())
            {
                Double money = 0.0;
                if (region.Equals(Constants.MB))
                {
                    money = getMoney(typePlay);
                    if (money == 0.0)
                    {
                        clearPlayer(thisPlayer);
                        return;
                    }
                    bl.tinh(thisPlayer, strNum, money, this.regionMB);
                }
                else if (!String.IsNullOrEmpty(daiMN))
                {
                    if (!this.dicDaiChinh.ContainsKey(daiMN) && !this.dicDaiPhu.ContainsKey(daiMN))
                    {
                        clearPlayer(thisPlayer);
                        thisPlayer.status = String.Format(Constants.WRONG_PROVINCE, daiMN);
                        return;
                    }

                    if (this.dicDaiChinh.ContainsKey(daiMN))
                    {
                        bl.tinh(thisPlayer, strNum, money, this.regionMN_DC);
                    }
                    else
                    {
                        bl.tinh(thisPlayer, strNum, money, this.regionMN_DP);
                    }
                }
                else
                {
                    bl.tinh(thisPlayer, strNum, money, this.regionMN_DC, this.regionMN_DP);
                }

                if (thisPlayer.result.Contains('*'))
                {
                    thisPlayer.result = thisPlayer.result.Replace("*", Constants.BL);
                }

            }
        }

        private static void clearPlayer(Player thisPlayer)
        {
            thisPlayer.status = Constants.ERROR;
            thisPlayer.result = String.Empty;
            thisPlayer.totalMoneyXac = 0;
            thisPlayer.totalMoneyXac2Digit = 0;
            thisPlayer.totalMoneyXac3Digit = 0;
            thisPlayer.totalMoneyXac4Digit = 0;
            thisPlayer.totalMoneyWin = 0;
            thisPlayer.totalMoneyWin2Digit = 0;
            thisPlayer.totalMoneyWin3Digit = 0;
            thisPlayer.totalMoneyWin4Digit = 0;
        }

        private static Double getMoney(String typePlay)
        {
            Double money = 0.0;

            //get tien trong loai choi
            var strmoney = typePlay.Remove(0, regexTP.Match(typePlay).Value.Length);
            if (strmoney.Contains(','))
            {
                strmoney = strmoney.Replace(',', '.');
            }

            if (strmoney.Contains('n'))
            {
                if (strmoney.LastIndexOf('n') == strmoney.Length - 1)
                {
                    strmoney = strmoney.Remove(strmoney.LastIndexOf('n'));
                }
            }

            try
            {
                money = Double.Parse(strmoney.ToString(), System.Globalization.NumberStyles.Currency);
            }
            catch (FormatException)
            {
                return money;
            }
            return money;
        }

        private static int getSoKeo(string number_one, string number_tow)
        {
            for (int i = 0; i < 3; i++)
            {
                if (number_one[i].Equals(number_tow[i]))
                {
                    continue;
                }
                else
                {
                    var a = int.Parse(number_one[i].ToString());
                    var b = int.Parse(number_tow[i].ToString());
                    if (a > b) 
                    {
                        return -1;
                    }
                    if (i == 0)
                    {
                        return 100;
                    }
                    if (i == 1)
                    {
                        return 10;
                    }
                    if (i == 2)
                    {
                        return 1;
                    }
                }
            }
            return -1;
        }
    }
}
