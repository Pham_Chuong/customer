﻿namespace Lottery_Statistics
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lb_Re4Con = new System.Windows.Forms.Label();
            this.lb_4Con = new System.Windows.Forms.Label();
            this.lb_ReTX4Con = new System.Windows.Forms.Label();
            this.lab_TongXac4Con = new System.Windows.Forms.Label();
            this.lb_ReTX3Con = new System.Windows.Forms.Label();
            this.lb_ReTX = new System.Windows.Forms.Label();
            this.lab_TongXac3Con = new System.Windows.Forms.Label();
            this.lab_TongXac2Con = new System.Windows.Forms.Label();
            this.lb_ReTC = new System.Windows.Forms.Label();
            this.lb_ReTX2Con = new System.Windows.Forms.Label();
            this.lb_Re3Con = new System.Windows.Forms.Label();
            this.lb_Re2Con = new System.Windows.Forms.Label();
            this.lab_TongXac = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lb_2Con = new System.Windows.Forms.Label();
            this.lb_3Con = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.rad_MB = new System.Windows.Forms.RadioButton();
            this.rad_MN = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lb_FileName = new System.Windows.Forms.Label();
            this.lb_FileNameVal = new System.Windows.Forms.Label();
            this.btn_Calc1 = new System.Windows.Forms.Button();
            this.btn_ChooseFile = new System.Windows.Forms.Button();
            this.btn_Exit = new System.Windows.Forms.Button();
            this.btn_XF = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.strMsgDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalMoneyXacDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.playerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Text Files (.txt)|*.txt";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(980, 79);
            this.label1.TabIndex = 0;
            this.label1.Text = "PHẦN MỀM THỐNG KÊ KẾT QUẢ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lb_Re4Con);
            this.groupBox3.Controls.Add(this.lb_4Con);
            this.groupBox3.Controls.Add(this.lb_ReTX4Con);
            this.groupBox3.Controls.Add(this.lab_TongXac4Con);
            this.groupBox3.Controls.Add(this.lb_ReTX3Con);
            this.groupBox3.Controls.Add(this.lb_ReTX);
            this.groupBox3.Controls.Add(this.lab_TongXac3Con);
            this.groupBox3.Controls.Add(this.lab_TongXac2Con);
            this.groupBox3.Controls.Add(this.lb_ReTC);
            this.groupBox3.Controls.Add(this.lb_ReTX2Con);
            this.groupBox3.Controls.Add(this.lb_Re3Con);
            this.groupBox3.Controls.Add(this.lb_Re2Con);
            this.groupBox3.Controls.Add(this.lab_TongXac);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.lb_2Con);
            this.groupBox3.Controls.Add(this.lb_3Con);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(74, 475);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(834, 185);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Thống kê";
            // 
            // lb_Re4Con
            // 
            this.lb_Re4Con.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Re4Con.ForeColor = System.Drawing.Color.Black;
            this.lb_Re4Con.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lb_Re4Con.Location = new System.Drawing.Point(93, 91);
            this.lb_Re4Con.Name = "lb_Re4Con";
            this.lb_Re4Con.Size = new System.Drawing.Size(183, 26);
            this.lb_Re4Con.TabIndex = 34;
            this.lb_Re4Con.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lb_4Con
            // 
            this.lb_4Con.AutoSize = true;
            this.lb_4Con.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_4Con.Location = new System.Drawing.Point(21, 96);
            this.lb_4Con.Name = "lb_4Con";
            this.lb_4Con.Size = new System.Drawing.Size(48, 16);
            this.lb_4Con.TabIndex = 33;
            this.lb_4Con.Text = "4 Con:";
            // 
            // lb_ReTX4Con
            // 
            this.lb_ReTX4Con.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ReTX4Con.ForeColor = System.Drawing.Color.Black;
            this.lb_ReTX4Con.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lb_ReTX4Con.Location = new System.Drawing.Point(683, 94);
            this.lb_ReTX4Con.Name = "lb_ReTX4Con";
            this.lb_ReTX4Con.Size = new System.Drawing.Size(145, 26);
            this.lb_ReTX4Con.TabIndex = 32;
            this.lb_ReTX4Con.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lb_ReTX4Con.UseWaitCursor = true;
            // 
            // lab_TongXac4Con
            // 
            this.lab_TongXac4Con.AutoSize = true;
            this.lab_TongXac4Con.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_TongXac4Con.Location = new System.Drawing.Point(543, 99);
            this.lab_TongXac4Con.Name = "lab_TongXac4Con";
            this.lab_TongXac4Con.Size = new System.Drawing.Size(139, 16);
            this.lab_TongXac4Con.TabIndex = 31;
            this.lab_TongXac4Con.Text = "Tổng tiền xác 4 Con:";
            // 
            // lb_ReTX3Con
            // 
            this.lb_ReTX3Con.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ReTX3Con.ForeColor = System.Drawing.Color.Black;
            this.lb_ReTX3Con.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lb_ReTX3Con.Location = new System.Drawing.Point(683, 59);
            this.lb_ReTX3Con.Name = "lb_ReTX3Con";
            this.lb_ReTX3Con.Size = new System.Drawing.Size(145, 26);
            this.lb_ReTX3Con.TabIndex = 30;
            this.lb_ReTX3Con.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lb_ReTX3Con.UseWaitCursor = true;
            // 
            // lb_ReTX
            // 
            this.lb_ReTX.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ReTX.ForeColor = System.Drawing.Color.Black;
            this.lb_ReTX.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lb_ReTX.Location = new System.Drawing.Point(645, 127);
            this.lb_ReTX.Name = "lb_ReTX";
            this.lb_ReTX.Size = new System.Drawing.Size(183, 26);
            this.lb_ReTX.TabIndex = 29;
            this.lb_ReTX.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lb_ReTX.UseWaitCursor = true;
            // 
            // lab_TongXac3Con
            // 
            this.lab_TongXac3Con.AutoSize = true;
            this.lab_TongXac3Con.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_TongXac3Con.Location = new System.Drawing.Point(541, 64);
            this.lab_TongXac3Con.Name = "lab_TongXac3Con";
            this.lab_TongXac3Con.Size = new System.Drawing.Size(139, 16);
            this.lab_TongXac3Con.TabIndex = 27;
            this.lab_TongXac3Con.Text = "Tổng tiền xác 3 Con:";
            // 
            // lab_TongXac2Con
            // 
            this.lab_TongXac2Con.AutoSize = true;
            this.lab_TongXac2Con.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_TongXac2Con.Location = new System.Drawing.Point(541, 30);
            this.lab_TongXac2Con.Name = "lab_TongXac2Con";
            this.lab_TongXac2Con.Size = new System.Drawing.Size(139, 16);
            this.lab_TongXac2Con.TabIndex = 26;
            this.lab_TongXac2Con.Text = "Tổng tiền xác 2 Con:";
            // 
            // lb_ReTC
            // 
            this.lb_ReTC.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ReTC.ForeColor = System.Drawing.Color.Black;
            this.lb_ReTC.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lb_ReTC.Location = new System.Drawing.Point(139, 127);
            this.lb_ReTC.Name = "lb_ReTC";
            this.lb_ReTC.Size = new System.Drawing.Size(137, 26);
            this.lb_ReTC.TabIndex = 25;
            this.lb_ReTC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lb_ReTX2Con
            // 
            this.lb_ReTX2Con.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ReTX2Con.ForeColor = System.Drawing.Color.Black;
            this.lb_ReTX2Con.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lb_ReTX2Con.Location = new System.Drawing.Point(683, 25);
            this.lb_ReTX2Con.Name = "lb_ReTX2Con";
            this.lb_ReTX2Con.Size = new System.Drawing.Size(145, 26);
            this.lb_ReTX2Con.TabIndex = 24;
            this.lb_ReTX2Con.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lb_ReTX2Con.UseWaitCursor = true;
            // 
            // lb_Re3Con
            // 
            this.lb_Re3Con.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Re3Con.ForeColor = System.Drawing.Color.Black;
            this.lb_Re3Con.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lb_Re3Con.Location = new System.Drawing.Point(93, 60);
            this.lb_Re3Con.Name = "lb_Re3Con";
            this.lb_Re3Con.Size = new System.Drawing.Size(183, 26);
            this.lb_Re3Con.TabIndex = 23;
            this.lb_Re3Con.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lb_Re2Con
            // 
            this.lb_Re2Con.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Re2Con.ForeColor = System.Drawing.Color.Black;
            this.lb_Re2Con.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lb_Re2Con.Location = new System.Drawing.Point(93, 29);
            this.lb_Re2Con.Name = "lb_Re2Con";
            this.lb_Re2Con.Size = new System.Drawing.Size(183, 26);
            this.lb_Re2Con.TabIndex = 22;
            this.lb_Re2Con.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lab_TongXac
            // 
            this.lab_TongXac.AutoSize = true;
            this.lab_TongXac.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_TongXac.Location = new System.Drawing.Point(543, 132);
            this.lab_TongXac.Name = "lab_TongXac";
            this.lab_TongXac.Size = new System.Drawing.Size(100, 16);
            this.lab_TongXac.TabIndex = 19;
            this.lab_TongXac.Text = "Tổng tiền xác :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 16);
            this.label5.TabIndex = 20;
            this.label5.Text = "Tiền chung :";
            // 
            // lb_2Con
            // 
            this.lb_2Con.AutoSize = true;
            this.lb_2Con.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_2Con.Location = new System.Drawing.Point(21, 34);
            this.lb_2Con.Name = "lb_2Con";
            this.lb_2Con.Size = new System.Drawing.Size(48, 16);
            this.lb_2Con.TabIndex = 15;
            this.lb_2Con.Text = "2 Con:";
            // 
            // lb_3Con
            // 
            this.lb_3Con.AutoSize = true;
            this.lb_3Con.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_3Con.Location = new System.Drawing.Point(21, 65);
            this.lb_3Con.Name = "lb_3Con";
            this.lb_3Con.Size = new System.Drawing.Size(48, 16);
            this.lb_3Con.TabIndex = 16;
            this.lb_3Con.Text = "3 Con:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(74, 182);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(834, 284);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kết quả";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.strMsgDataGridViewTextBoxColumn,
            this.totalMoneyXacDataGridViewTextBoxColumn,
            this.resultDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.playerBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Beige;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(3, 22);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.Size = new System.Drawing.Size(828, 259);
            this.dataGridView1.TabIndex = 8;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // rad_MB
            // 
            this.rad_MB.Checked = true;
            this.rad_MB.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.rad_MB.Location = new System.Drawing.Point(83, 40);
            this.rad_MB.Name = "rad_MB";
            this.rad_MB.Size = new System.Drawing.Size(85, 25);
            this.rad_MB.TabIndex = 11;
            this.rad_MB.TabStop = true;
            this.rad_MB.Text = "Miền Bắc";
            this.rad_MB.UseVisualStyleBackColor = true;
            // 
            // rad_MN
            // 
            this.rad_MN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.rad_MN.Location = new System.Drawing.Point(226, 40);
            this.rad_MN.Name = "rad_MN";
            this.rad_MN.Size = new System.Drawing.Size(96, 25);
            this.rad_MN.TabIndex = 12;
            this.rad_MN.TabStop = true;
            this.rad_MN.Text = "Miền Nam";
            this.rad_MN.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lb_FileName);
            this.groupBox1.Controls.Add(this.lb_FileNameVal);
            this.groupBox1.Controls.Add(this.rad_MN);
            this.groupBox1.Controls.Add(this.rad_MB);
            this.groupBox1.Controls.Add(this.btn_Calc1);
            this.groupBox1.Controls.Add(this.btn_ChooseFile);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(74, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(834, 98);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dữ liệu";
            // 
            // lb_FileName
            // 
            this.lb_FileName.AutoSize = true;
            this.lb_FileName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_FileName.Location = new System.Drawing.Point(80, 75);
            this.lb_FileName.Name = "lb_FileName";
            this.lb_FileName.Size = new System.Drawing.Size(69, 16);
            this.lb_FileName.TabIndex = 17;
            this.lb_FileName.Text = "Path File:";
            // 
            // lb_FileNameVal
            // 
            this.lb_FileNameVal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_FileNameVal.Location = new System.Drawing.Point(155, 75);
            this.lb_FileNameVal.MaximumSize = new System.Drawing.Size(600, 16);
            this.lb_FileNameVal.Name = "lb_FileNameVal";
            this.lb_FileNameVal.Size = new System.Drawing.Size(600, 16);
            this.lb_FileNameVal.TabIndex = 16;
            // 
            // btn_Calc1
            // 
            this.btn_Calc1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Calc1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Calc1.Location = new System.Drawing.Point(713, 40);
            this.btn_Calc1.Name = "btn_Calc1";
            this.btn_Calc1.Size = new System.Drawing.Size(75, 25);
            this.btn_Calc1.TabIndex = 10;
            this.btn_Calc1.Text = "Tính Toán";
            this.btn_Calc1.UseVisualStyleBackColor = true;
            this.btn_Calc1.Click += new System.EventHandler(this.btn_Calc1_Click);
            // 
            // btn_ChooseFile
            // 
            this.btn_ChooseFile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_ChooseFile.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ChooseFile.ForeColor = System.Drawing.Color.Red;
            this.btn_ChooseFile.Location = new System.Drawing.Point(575, 40);
            this.btn_ChooseFile.Name = "btn_ChooseFile";
            this.btn_ChooseFile.Size = new System.Drawing.Size(75, 25);
            this.btn_ChooseFile.TabIndex = 9;
            this.btn_ChooseFile.Text = "Chọn File";
            this.btn_ChooseFile.UseVisualStyleBackColor = true;
            this.btn_ChooseFile.Click += new System.EventHandler(this.btn_ChooseFile_Click);
            // 
            // btn_Exit
            // 
            this.btn_Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Exit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Exit.Location = new System.Drawing.Point(787, 675);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(75, 25);
            this.btn_Exit.TabIndex = 13;
            this.btn_Exit.Text = "Thoát";
            this.btn_Exit.UseVisualStyleBackColor = true;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // btn_XF
            // 
            this.btn_XF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_XF.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_XF.Location = new System.Drawing.Point(649, 675);
            this.btn_XF.Name = "btn_XF";
            this.btn_XF.Size = new System.Drawing.Size(75, 25);
            this.btn_XF.TabIndex = 12;
            this.btn_XF.Text = "Xuất File";
            this.btn_XF.UseVisualStyleBackColor = true;
            this.btn_XF.Click += new System.EventHandler(this.btn_XF_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Excel Workbook(*.xlsx)|";
            // 
            // strMsgDataGridViewTextBoxColumn
            // 
            this.strMsgDataGridViewTextBoxColumn.DataPropertyName = "strMsg";
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.strMsgDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.strMsgDataGridViewTextBoxColumn.HeaderText = "Tin Nhắn";
            this.strMsgDataGridViewTextBoxColumn.Name = "strMsgDataGridViewTextBoxColumn";
            this.strMsgDataGridViewTextBoxColumn.Width = 250;
            // 
            // totalMoneyXacDataGridViewTextBoxColumn
            // 
            this.totalMoneyXacDataGridViewTextBoxColumn.DataPropertyName = "totalMoneyXac";
            this.totalMoneyXacDataGridViewTextBoxColumn.HeaderText = "Tổng Tiền";
            this.totalMoneyXacDataGridViewTextBoxColumn.Name = "totalMoneyXacDataGridViewTextBoxColumn";
            this.totalMoneyXacDataGridViewTextBoxColumn.Width = 150;
            // 
            // resultDataGridViewTextBoxColumn
            // 
            this.resultDataGridViewTextBoxColumn.DataPropertyName = "result";
            this.resultDataGridViewTextBoxColumn.HeaderText = "Kết Quả";
            this.resultDataGridViewTextBoxColumn.Name = "resultDataGridViewTextBoxColumn";
            this.resultDataGridViewTextBoxColumn.Width = 250;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "Ghi Chú";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            this.statusDataGridViewTextBoxColumn.Width = 150;
            // 
            // playerBindingSource
            // 
            this.playerBindingSource.DataSource = typeof(Lottery_Statistics.Player);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 722);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Exit);
            this.Controls.Add(this.btn_XF);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phần mềm thống kê";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lab_TongXac;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lb_2Con;
        private System.Windows.Forms.Label lb_3Con;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.RadioButton rad_MB;
        private System.Windows.Forms.RadioButton rad_MN;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_Calc1;
        private System.Windows.Forms.Button btn_ChooseFile;
        private System.Windows.Forms.Button btn_Exit;
        private System.Windows.Forms.Button btn_XF;
        private System.Windows.Forms.Label lb_Re3Con;
        private System.Windows.Forms.Label lb_Re2Con;
        private System.Windows.Forms.Label lb_ReTX2Con;
        private System.Windows.Forms.Label lb_ReTC;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.BindingSource playerBindingSource;
        private System.Windows.Forms.Label lb_ReTX3Con;
        private System.Windows.Forms.Label lb_ReTX;
        private System.Windows.Forms.Label lab_TongXac3Con;
        private System.Windows.Forms.Label lab_TongXac2Con;
        private System.Windows.Forms.DataGridViewTextBoxColumn strMsgDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalMoneyXacDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn resultDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label lb_ReTX4Con;
        private System.Windows.Forms.Label lab_TongXac4Con;
        private System.Windows.Forms.Label lb_4Con;
        private System.Windows.Forms.Label lb_Re4Con;
        private System.Windows.Forms.Label lb_FileName;
        private System.Windows.Forms.Label lb_FileNameVal;
    }
}
