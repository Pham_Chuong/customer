﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
namespace Lottery_Statistics
{
    class BaoLo : IDisposable
    {
        public void Dispose() {
            GC.SuppressFinalize(this);
        }

        public void tinh(Player thisPlayer, String digitVal, Double money, Region regionData1 = null, Region regionData2 = null, Boolean is7Lo = false)
        {
            ArrayList arrList1 = null;
            ArrayList arrList2 = null;

            switch (digitVal.ToString().Length)
            {
                case 2:
                    //tính tổng tiền xác của số 2 digit
                    arrList1 = regionData1.arr2Digit;
                    if (regionData2 != null)
                    {
                        arrList2 = regionData2.arr2Digit;
                    }
                    thisPlayer.totalMoneyXac2Digit += money * (arrList1.Count + (arrList2 == null ? 0 : arrList2.Count));
                    break;
                case 3:
                    
                    if (is7Lo)
                    {
                        arrList1 = new ArrayList();
                        arrList2 = new ArrayList();
                        //tính tổng tiền xác của số 3 digit
                        if (regionData1.arr3Digit.Count == 23) {
                            arrList1.Add(regionData1.arr3Digit[0]);
                            arrList1.Add(regionData1.arr3Digit[17]);
                            arrList1.Add(regionData1.arr3Digit[18]);
                            arrList1.Add(regionData1.arr3Digit[19]);
                            arrList1.Add(regionData1.arr3Digit[20]);
                            arrList1.Add(regionData1.arr3Digit[21]);
                            arrList1.Add(regionData1.arr3Digit[22]);
                        }else 
                        {
                            arrList1.Add(regionData1.arr3Digit[0]);
                            arrList1.Add(regionData1.arr3Digit[11]);
                            arrList1.Add(regionData1.arr3Digit[12]);
                            arrList1.Add(regionData1.arr3Digit[13]);
                            arrList1.Add(regionData1.arr3Digit[14]);
                            arrList1.Add(regionData1.arr3Digit[15]);
                            arrList1.Add(regionData1.arr3Digit[16]);
                        }
                        
                        if (regionData2 != null)
                        {
                            arrList2.Add(regionData2.arr3Digit[0]);
                            arrList2.Add(regionData2.arr3Digit[11]);
                            arrList2.Add(regionData2.arr3Digit[12]);
                            arrList2.Add(regionData2.arr3Digit[13]);
                            arrList2.Add(regionData2.arr3Digit[14]);
                            arrList2.Add(regionData2.arr3Digit[15]);
                            arrList2.Add(regionData2.arr3Digit[16]);
                        }
                        thisPlayer.totalMoneyXac3Digit += money * (arrList1.Count + (arrList2 == null ? 0 : arrList2.Count));
                    }
                    else
                    {
                       //tính tổng tiền xác của số 3 digit
                        arrList1 = regionData1.arr3Digit;
                        if (regionData2 != null)
                        {
                            arrList2 = regionData2.arr3Digit;
                        }
                        thisPlayer.totalMoneyXac3Digit += money * (arrList1.Count + (arrList2 == null ? 0 : arrList2.Count));
                    }
                    
                    break;
                case 4:
                    //tính tổng tiền xác của số 4 digit
                    arrList1 = regionData1.arr4Digit;
                    if (regionData2 != null)
                    {
                        arrList2 = regionData2.arr4Digit;
                    }
                    thisPlayer.totalMoneyXac4Digit += money * (arrList1.Count + (arrList2 == null ? 0 : arrList2.Count));
                    break;
            }

            //Count value digitVal in regionData
            var query1 = from string strtmp in arrList1
                          where strtmp.Equals(digitVal)
                          select strtmp;


            var query2 = from string strtmp in (arrList2 == null ? new ArrayList() : arrList2)
                             where strtmp.Equals(digitVal)
                             select strtmp;


            if (query1.Count() + query2.Count() > 0)
            {
                switch (digitVal.ToString().Length)
                {
                    case 2:
                        //tính tổng tiền thắng của số 2 digit
                        thisPlayer.totalMoneyWin2Digit += money * (query1.Count() + query2.Count()) * 75;
                        break;
                    case 3:
                        //tính tổng tiền thắng của số 3 digit
                        thisPlayer.totalMoneyWin3Digit += money * (query1.Count() + query2.Count()) * 650;
                        break;
                    case 4:
                        //tính tổng tiền thắng của số 4 digit
                        thisPlayer.totalMoneyWin4Digit += money * (query1.Count() + query2.Count()) * 6000;
                        break;
                }

                thisPlayer.result += String.Format(Constants.FORMAT_RESULT, new object[] { digitVal, query1.Count() + query2.Count() });
                if (String.IsNullOrEmpty(thisPlayer.status) || !thisPlayer.status.Equals(Constants.WIN))
                {
                    thisPlayer.status = Constants.WIN;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(thisPlayer.status) || !thisPlayer.status.Equals(Constants.WIN))
                {
                    thisPlayer.status = Constants.LOSE;
                }
            }
        }            
    }
}
