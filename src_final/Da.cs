﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Lottery_Statistics
{
    class Da : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void tinh(Player thisPlayer, ArrayList arrDigit, Double money, Region regionData1 = null, Region regionData2 = null, Boolean isDaCap = false)
        {
            ArrayList arrList1 = null;
            ArrayList arrList2 = null;
            int coutNum = 0;
            if (regionData2 != null)
            {
                coutNum = 72;
            }
            else
            {
                if (regionData1.arr2Digit.Count > 17)
                {
                    coutNum = 54;
                }
                else 
                {
                    coutNum = 36;
                }
            }

            List<ArrayList> listDa = new List<ArrayList>();
            if (isDaCap == false)
            {
                switch (arrDigit.Count)
                {
                    case 2:
                        listDa.Add(arrDigit);
                        break;
                    default:
                        for (int i = 0; i < arrDigit.Count - 1; i+=2)
                        {
                            ArrayList tmpDig = new ArrayList();
                            tmpDig.Add(arrDigit[i]);
                            tmpDig.Add(arrDigit[i+1]);
                            listDa.Add(tmpDig);
                        }
                        break;
                }
            }
            else
            {
                switch (arrDigit.Count)
                {
                    case 2:
                        listDa.Add(arrDigit);
                        break;
                    default:
                        for (int i = 0; i < arrDigit.Count - 3; i+=2)
                        {
                            for (int j = i + 1; j < arrDigit.Count-1; j++)
                            {
                                ArrayList tmpDig = new ArrayList();
                                tmpDig.Add(arrDigit[i]);
                                tmpDig.Add(arrDigit[j]);
                                listDa.Add(tmpDig);
                            }
                        }
                        break;
                }
            }
            

            switch (arrDigit[0].ToString().Length)
            {
                case 2:
                    //tính tổng tiền xác của số 2 digit
                    arrList1 = regionData1.arr2Digit;
                    if (regionData2 != null)
                    {
                        arrList2 = regionData2.arr2Digit;
                    }
                    thisPlayer.totalMoneyXac2Digit += money * listDa.Count * coutNum;
                    break;
                case 3:
                    //tính tổng tiền xác của số 3 digit
                    arrList1 = regionData1.arr3Digit;
                    if (regionData2 != null)
                    {
                        arrList2 = regionData2.arr3Digit;
                    }
                    thisPlayer.totalMoneyXac3Digit += money * listDa.Count * coutNum;
                    break;
            }

            foreach (var arr in listDa)
            {
                //tim cap so da trong dai thu nhat
                var query1 = from string strtmp in arrList1
                             where strtmp.Equals(arr[0])
                             select strtmp;
                var query2 = from string strtmp in (arrList2 == null ? new ArrayList() : arrList2)
                             where strtmp.Equals(arr[0])
                             select strtmp;

                var query3 = from string strtmp in arrList1
                             where strtmp.Equals(arr[1])
                             select strtmp;
                var query4 = from string strtmp in (arrList2 == null ? new ArrayList() : arrList2)
                             where strtmp.Equals(arr[1])
                             select strtmp;

                if (query1.Count() + query2.Count() > 0 && query3.Count() + query4.Count() > 0)
                {
                    var min = Math.Min(query1.Count() + query2.Count(), query3.Count() + query4.Count());
                    switch (arr[0].ToString().Length)
                    {
                        case 2:
                            //tính tổng tiền thắng của số 2 digit
                            thisPlayer.totalMoneyWin2Digit += money * min * (arrList2 == null ? 750 : 580);
                            break;
                        case 3:
                            //tính tổng tiền thắng của số 3 digit
                            thisPlayer.totalMoneyWin3Digit += money * min * (arrList2 == null ? 750 : 580);
                            break;
                    }
                    thisPlayer.result += String.Format(Constants.FORMAT_RESULT, new object[] { arr[0] + "." + arr[1], min });
                    if (String.IsNullOrEmpty(thisPlayer.status) || !thisPlayer.status.Equals(Constants.WIN))
                    {
                        thisPlayer.status = Constants.WIN;
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(thisPlayer.status) || !thisPlayer.status.Equals(Constants.WIN))
                    {
                        thisPlayer.status = Constants.LOSE;
                    }
                }
            }
        }
    }
}
