﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Lottery_Statistics
{
    class Permutation_Generation
    {
        /*
         *  Convert char to String !
         */
        static string converChartoString(char value)
        {
            return new string(value, 1);
        }

        public static IList<string> Permutation(String s)
        {
            List<string> list = new List<string>();
            for (int i = 0; i < s.Length; i++)
            {
                list.Add(converChartoString(s[i]));
            }
            IList<string> perms = Permutations(list);
            return perms;
        }

        /*
         *  Prossce .
         */
        public static IList<string> Permutations<T>(IList<T> list)
        {
            List<string> perms = new List<string>();
            if (list.Count == 0)
                return perms; // Empty list.
            int factorial = 1;
            for (int i = 2; i <= list.Count; i++)
                factorial *= i;
            for (int v = 0; v < factorial; v++)
            {
                List<T> s = new List<T>(list);
                int k = v;
                for (int j = 2; j <= list.Count; j++)
                {
                    int other = (k % j);
                    T temp = s[j - 1];
                    s[j - 1] = s[other];
                    s[other] = temp;
                    k = k / j;
                }

                string tmp = String.Empty;
                foreach (var stmp in s)
                {
                    tmp += stmp;
                }
                if (!perms.Contains(tmp))
                {
                    perms.Add(tmp);
                }
            }
            return perms;
        }
    }
}
